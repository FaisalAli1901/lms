import { Template } from 'meteor/templating';


Template.navbar.events({
    'click #chat': (e) => {
        e.preventDefault();
        Modal.show('chatModel');
    },
    'click #discussionForm': (e) => {
        e.preventDefault();
        Modal.show('discussionForm');
    },
    'click #logout': (e) => {
        e.preventDefault();
        Meteor.logout();
        Router.go('/');
    }
});