import { Template } from 'meteor/templating';

var userName = 'Anonymous';

Template.messages.helpers({
    messages() {
        return Messages.find({}, { sort: { time: 1 } });
    }
});

Template.input.events({
    'click .send'(e) {
        e.preventDefault();
        var message = document.getElementById("message").value

        if (message != "") {
            Messages.insert({
                name: userName,
                message: message,
                time: Date.now()
            });
        }

        document.getElementById('message').value = '';
        message.value = '';
    }
});

Template.welcome.events({
    "click .setname"(e) {
        e.preventDefault();
        userName = document.getElementById("name").value
        console.log(userName);

        document.getElementById('name').value = '';
    }
});