import { Template } from 'meteor/templating';

import VideoPeer from '../functions/videopeer';

var videoPeer;
var userId;

Template.videoChat.onRendered(() => {
    userId = Router.current().params.id
    videoPeer = new VideoPeer();
})

Template.videoChat.onDestroyed(() => {
    videoPeer.stopMediastream();
})

Template.callzone.onCreated(() => {
    Meteor.subscribe('users');
})

Template.callzone.events({
    'click .callaction': (e, t) => {
        e.preventDefault();

        let user = Meteor.users.find({ _id: userId }).fetch()
        let key = user[0].profile.peerId

        videoPeer.callAKey(key);

        return false;
    }
});

Template.hangup.events({
    'click .hangupaction': (e, t) => {
        e.preventDefault();
        videoPeer.hangup();
        return false;
    }
});