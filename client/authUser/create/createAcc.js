import { Template } from 'meteor/templating';
import { Accounts } from 'meteor/accounts-base'

let profileImageUrl = ''

Template.signup.onDestroyed(() => {
	// Session.set('userProfile', 'default')
})

Template.signup.onCreated(() => {
	template = Template.instance();
	template.uploadId = new ReactiveVar();
	template.uploading = new ReactiveVar(false);
})

Template.signup.onRendered(() => {
	Session.set('userProfile', 'default')
})

Template.signup.helpers({
	userPic() {
		// let profileVal = Session.get('userProfile')
		// let profile = !!profileVal
		// console.log(profileImageUrl);
		// if(profile){
		// 	if (profileVal !== "default") {
		// 		return profileImageUrl
		// 	} else {
		// 		return '/login/default_profile_pic.jpg'
		// 	}
		// }else{
		// 	return '/login/default_profile_pic.jpg'
		// }

		let imgUploaded = Template.instance().uploading.get();

		if(imgUploaded){
			return Template.instance().uploadId.get()
		}else{
			return '/login/default_profile_pic.jpg'
		}
	}
});

Template.signup.events({
	'click .signupBtn': (e, t) => {
		e.preventDefault();
		let firstName = t.find("#firstName").value
		let lastName = t.find("#lastName").value
		let username = t.find("#username").value
		let email = t.find("#emailId").value
		let password = t.find("#password").value
		let profilePic = ''

		if(profileImageUrl){
			profilePic = profileImageUrl
		}else{
			profilePic = '/login/default_profile_pic.jpg'
		}

		if(firstName && lastName && email && password && username){
			console.log('notEmpty');

			Accounts.createUser({
				username,
				email,
				password,
				profile: {
					firstName,
					lastName,
					pic: profilePic
				}
			}, (err, success) => {
				if(err){
					console.log(err);
				}else{
					console.log('Login Successfull');
					Router.go('/')
				}
			})
		}
	},
	'change #profile_pic': (e, t) => {
		FS.Utility.eachFile(event, (file) => {
			Images.insert(file, (err, fileObj) => {
				if(!err){
					profileImageUrl = '/cfs/files/images/' + fileObj._id
					console.log(profileImageUrl);
					setTimeout(() => {
						// Session.set('userProfile', 'userpic')
						template.uploadId.set(profileImageUrl);
						template.uploading.set(true);
					}, 200);
				}else{
					console.log(error);
				}
			})
		})
	}
});