import { Template } from 'meteor/templating';

Template.login.helpers({ 

}); 

Meteor.startup(function () {
	// Client startup method.
	Meteor.absoluteUrl.defaultOptions.rootUrl = 'http://localhost:3000';
});

Template.login.events({ 
	'click .facebookLogin': (e, t) => { 
		e.preventDefault();
		Meteor.loginWithFacebook({
			requestPermissions: ['public_profile', 'email']
		}, (err) => {
			if (err) {
				console.log(err);
			} else {
				console.log('Login Successful');
				Router.go('/');
			}
		});
	},
	'click .googleLogin': (e, t) => {
		e.preventDefault();
		Meteor.loginWithGoogle({
			requestPermissions: ['email']
		}, (err) => {
			if (err) {
				console.log(err);
			} else {
				console.log('Login Successful');
				Router.go('/');
			}
		});
	} 
}); 
