import { Template } from 'meteor/templating';

Template.loginWithEmail.events({ 
	'click .signIn': (e, t) => { 
		e.preventDefault();
		let emailId = t.find('#username').value
		let password = t.find('#password').value

		Meteor.loginWithPassword(emailId, password, (err, success) => {
			if(err){
				console.log(err);
			}else{
				console.log('Login Successful');
				Router.go('/');
			}
		})
	} 
}); 
