Template.registerHelper('isCurrentUser', ( author ) => {
  Meteor.subscribe("users");
  let currentUser = Meteor.user();
  if (currentUser.emails[0].address == author.emails[0].address) {
    return true;
  }
});

Template.registerHelper('getCurrentUserDetails', ( ) => {
  Meteor.subscribe("users");
  let userDetails = Meteor.user();
  return userDetails;
});

Template.registerHelper('getUserDetails', ( user ) => {
  Meteor.subscribe("users");
  if ( user ) {
    Meteor.subscribe('users', user);
    let userDetails = Meteor.users.findOne({
      "emails.address": user
    });
    return userDetails;
  }
});

Template.registerHelper('getUserDetailsById', ( user_id ) => {
  Meteor.subscribe("users");
  if ( user_id ) {
    let userDetails = Meteor.users.findOne({
      "_id": user_id
	});
    return userDetails;
  }
});

UI.registerHelper('limitPara', ( para, length ) => {
  let str = ""
  if(typeof(para) == "string") {
    str = para;
  } else {
    str = $(para).text();
  }

  if (str.length > length) {
    str = str.substring(0, length) + ' ...'
    return str
  } else {
    return str
  }
});