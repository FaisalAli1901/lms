import { Template } from 'meteor/templating';
import isHtml from 'is-html'

let totalDiscussing = 0;

Template.resourcesCards.onCreated(() => {
  Meteor.subscribe('resources');
  Meteor.subscribe('users');
});


Template.resourcesCards.helpers({
	getResources() {
    let resources = Resource.find().fetch();
    if(resources){
      return resources
    }
  },
	getTemplate(data, index){
		let templates = ["card1", "card2", "card3", "card4", "card5", "card6", "card7"]

		if(data.coverPic === ''){
			return {
				temp: 'card2',
				data: data
			}
		}else{
			return {
				temp: 'card1',
				data: data
			}
		}
	}
});

Template.card1.helpers({
	getUserImg(index, userId) {
		if (index > 1) {
			totalDiscussing++
		} else {
			if (index === 0) {
				return `<img class="top" src="/home/discussion-author.png" alt="user pic">`
			} else {
				return `<img src="/home/discussion-comment.png" alt="user pic">`
			}
		}
	},
	getUserName() {
		return {
			name: 'Rahil Khan, Alex Wilson',
			count: totalDiscussing
		}
	},
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;

		if (str.length > 180) {
			console.log(str);
			str = str.substring(0, 180) + ' ...'
			return str
		} else {
			return str
		}

	}
});

Template.card1WithoutImg.helpers({
	getUserImg(index, userId) {
		if (index > 1) {
			totalDiscussing++
		} else {
			if (index === 0) {
				return `<img class="top" src="/home/discussion-author.png" alt="user pic">`
			} else {
				return `<img src="/home/discussion-comment.png" alt="user pic">`
			}
		}
	},
	getUserName() {
		return {
			name: 'Rahil Khan, Alex Wilson',
			count: totalDiscussing
		}
	},
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;
		if (str.length > 300) {
			console.log(str);
			str = str.substring(0, 300) + ' ...'
			return str
		} else {
			return str
		}

	}
});

Template.card2.helpers({
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;

		if (str.length > 90) {
			str = str.substring(0, 90) + '...'
			return str
		} else {
			return str
		}

	}
});

Template.card3.helpers({
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;
		if (str.length > 75) {
			str = str.substring(0, 75) + '...'
			return str
		} else {
			return str
		}

	}
});

Template.card4.helpers({
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;
		if (str.length > 250) {
			str = str.substring(0, 250) + '...'
			return str
		} else {
			return str
		}

	}
});

Template.card5.helpers({
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;
		if (str.length > 125) {
			str = str.substring(0, 125) + '...'
			return str
		} else {
			return str
		}
	},
	activeUsername(username){
		return `${username} is highly active on this resource`
	}
});

Template.card6.helpers({
	limitPara(para) {
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;
		if (str.length > 90) {
			str = str.substring(0, 90) + '...'
			return str
		} else {
			return str
		}

	}
});

Template.card7.helpers({
	limitPara(para){
		let str = isHtml(para) ? $(`<div>${para}</div>`).text() : para;
		if (str.length > 90){
			str = str.substring(0, 90) + '...'
			return str
		}else{
			return str
		}

	}
});

Template.resourcesCards.events({

});
