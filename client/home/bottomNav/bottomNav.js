import { Template } from 'meteor/templating';

Template.bottomNav.onRendered(() => {

	var feedbackFormHtml = Blaze.toHTML(Template.feedback);

	let feedbackPopover = $("#sendFeedback").popover({
		"html": 'true',
		"placement": 'top',
		"trigger": 'click',
		"container": 'body',
		title: function () {
			return '<span class="text-info"><strong>' + $(this).data('title') + '</strong></span>' + '<span class="close closeFeedbackPopover" data-dismiss="alert">&times;</span>';
		},
		"content": feedbackFormHtml
	});

	feedbackPopover.on("show.bs.popover", function (e) {
		var popover = $(this),
			w = popover.data("width"),
			h = popover.data("height");
		feedbackPopover.data("bs.popover").tip().css({
			width: w,
			position: 'fixed'
		});
	});

	$(document).on("click", ".popover .closeFeedbackPopover", function () {
		$(this).parents(".popover").popover('hide');
	});

	$(document).on('hidden.bs.popover', function (e) {
			$(e.target).data("bs.popover").inState.click = false;
	});

});

Template.bottomNav.events({
	'click #discussionForm': (e) => {
		e.preventDefault();
		Modal.show('discussionForm');
	},
	'click #searchBtn': (e) => {
		e.preventDefault();
		Modal.show('searchModal');
	},
	'click #logout': (e) => {
		e.preventDefault();
		Meteor.logout();
		Router.go('/');
	},
	'click #sendFeedback': (e) => {
		e.preventDefault();

		$(document).on("submit", "#feedbackForm", function (e) {
			e.preventDefault();

			let jForm = $(e.currentTarget),
				dataContext = {}
			currentUser = Meteor.user();

			if (currentUser) {
				dataContext = {
					"firstName": currentUser.profile.firstName,
					"lastName": currentUser.profile.lastName,
					"email": currentUser.emails[0].address,
					"body": jForm[0].body.value
				}
			} else {
				dataContext = jForm.serializeArray();
			}

			var html = Blaze.toHTMLWithData(Template.feedbackEmailContent, dataContext);
			var options = {
				from: "noreply@collectivhumanintelligence.org",
				to: "r.jain@unseco.org",
				subject: "CHI Feedback",
				html: html
			};

			Meteor.call("sendFeedbackEmail", options);
			let feedbackFormHeight = $("#feedbackForm").height()
			$('#feedbackForm').html("<div class='text text-success' style='height:"+ feedbackFormHeight +"px'> Successfully sent feedback. Thank you!</div>");

		});
	}
});