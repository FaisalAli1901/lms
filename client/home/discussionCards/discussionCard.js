import { Template } from 'meteor/templating';

let activeUser = 0;

Template.discussionCard.onCreated(() => {
	Meteor.subscribe('linkPreviews');
  Meteor.subscribe('discussions');
  Meteor.subscribe('users');
  Meteor.subscribe('threads');
});

Template.cardDiscussion.onCreated(() => {
	Meteor.subscribe('linkPreviews');
  Meteor.subscribe('discussions');
	Meteor.subscribe('users');
});

Template.discussionCard.onRendered(() => {
	Meteor.subscribe('linkPreviews');

	setTimeout(() => {
		var linkPreviews = $('.link-preview.unloaded');
		_.each(linkPreviews, (linkPreview) => {
			let qUrl = $(linkPreview).data("link");
			let objectUrl = LP.findOne({
				"url": qUrl
			});
			if (objectUrl) {
				$(linkPreview).find('.link-image').attr("src", objectUrl.image);
				$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(objectUrl.title, 100));
				$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(objectUrl.description, 200));
			} else {
				HTTP.call('GET', 'http://api.linkpreview.net/?key=59c881e95420219e6fdde211a88280424205b456ef721&q=' + qUrl, {}, function (error, response) {
					if (error) {
						console.log(error);
					} else {
						$(linkPreview).find('.link-image').attr("src", response.data.image);
						$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(response.data.title, 100));
						$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(response.data.description, 200));
						let lpData = {
							"url": qUrl,
							"image": response.data.image,
							"title": response.data.title,
							"description": response.data.description
						};
						Meteor.call('addLinkPreview', lpData, (result) => {
							console.log(result)
						});
					}
				});

			}
			$(linkPreview).removeClass('unloaded');
		});
	}, 2000);

})

Template.discussionCard.helpers({
  getDiscussion() {
    let discussion = Discussion.find().fetch();
    if(discussion){
      return discussion
    }
  }
});

Template.cardDiscussion.helpers({
  activeUsers(data){
    if(data.length > 1){
      return true;
    }else{
      return false;
    }
  },
  fetchThread(Id) {
    let thread = Thread.findOne({ _id: Id });
    if (thread) {
      return thread
    }
  },
  less_than_9(index) {
    if (index >= 9) {
      activeUser++
      return false
    } else {
      return true
    }
  },
	greater_than_9(index) {
    if (index < 9) {
      return false
    } else {
      return true
    }
  },
  getCount() {
    return activeUser;
  },
  limitBody(para, length) {
    let str = ""
    if(typeof(para) == "string") {
      str = para;
    } else {
      str = $(para).text();
    }

    if (str.length > length) {
      str = str.substring(0, length) + ' ...'
      return str
    } else {
      return str
    }
  },
  activeUser(users){
    if(users > 1){
      return true;
    }else{
      return false
    }
  }
});

