import { Template } from 'meteor/templating';

let slickSlider

Template.slickSlides.onCreated(() => {
	Meteor.subscribe('resources');
	Meteor.subscribe('discussions');
});

Template.slickSlides.onRendered(() => {

});

Template.slickSlides.events({
	'click .prev': (e, t) => {
		e.preventDefault();
		slickSlider.trigger('prev.owl.carousel', [300]);
	},
	'click .next': (e, t) => {
		e.preventDefault();
		slickSlider.trigger('next.owl.carousel', [300]);
	}
});