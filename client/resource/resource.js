import { Template } from 'meteor/templating';

Handlebars.registerHelper('splitTags', function (tags) {
  var t = tags.split(",");
  var tagsHtml = "";
  t.forEach((v, i) => {
    tagsHtml += '<a href="#">' + v + '</a>'
  })
  return tagsHtml;
});

Template.resource.onCreated(() => {
  Meteor.call('updateCurrentTemplate', "", "");
  Meteor.subscribe('users');
  Meteor.subscribe('resources');
  Meteor.subscribe('currentTemplate');
  Meteor.subscribe('BotConversation');
});

Template.resource.helpers({
  getResource() {
    let resource_id = Router.current().params.id;
    if (resource_id) {
      return Resource.findOne({ _id: resource_id });
    }
  },
  getTemplate() {
    Meteor.subscribe('currentTemplate');
    let temp = CurrentTemplate.findOne();
    let id = temp.id, type = temp.type;
    let data = { type: type, template: {} };

    if (type == "article") {
      Meteor.subscribe('articles');
      data.template = Articles.findOne({ _id: id });
      return data;
    }
    if (type == "question") {
      Meteor.subscribe('questions');
      data.template = Questions.findOne({ _id: id });
      return data;
    }
    if (type == "journaling") {
      Meteor.subscribe('journaling');
      data.template = Journaling.findOne({ _id: id });
      return data;
    }
    if (type == "bot") {
      let temp = BotConversation.findOne({ _id: id });
      data.template = temp.states.start.response
      return data;
    }
  }
});

Template.resource_footer.events({
  'click .like': function (e, template) {
    e.preventDefault();
    let resourceId = Router.current().params.id,
        userId = Meteor.user()._id;

    Meteor.call('updateResourceLike', resourceId, userId);
  },
  'click .bookmark': function (e, template) {
    e.preventDefault();
    let resourceId = Router.current().params.id,
        userId = Meteor.user()._id;

    Meteor.call('updateResourceBookmark', resourceId, userId);
  }
});

Template.resource.events({
	'click .logout': (e) => {
		e.preventDefault();
		Meteor.logout();
		Router.go('/');
	},
  'click #show-pages': (e) => {
    e.preventDefault();
    $('.toc-jumbotron').toggleClass('hidden');
  },
  'click .getContext': (e) => {
    e.preventDefault();

    let jqEl = $(e.currentTarget)
      , id = jqEl.data("id")
      , type = jqEl.data("type")
      ;

    $('.resource-cover-page').remove();
    $('.current-page-number').html($(e.target).closest('.item').prevAll('.item').length + 1);
    $('.item-content').removeClass('active');
    $(e.target).closest('.item-content').addClass('active');
    Meteor.call('updateCurrentTemplate', type, id);
  },
  'click .prev-page': (e) => {
    e.preventDefault();

    let itemContent = $('.item-content.active');
    if(itemContent.length > 0) {
      $(itemContent).closest('.item').prev('.item').find('.getContext').trigger('click');
    } else {
      $('.getContext').first().trigger('click');
    }
    $('.question-jumbotron').show();
    $('.feedback-jumbotron').hide();
    $('form').find('input[type="submit"]').val("Submit").removeClass('next-page');
  },
  'click .next-page': (e) => {
    e.preventDefault();
    let itemContent = $('.item-content.active');
    if(itemContent.length > 0) {
      $(itemContent).closest('.item').next('.item').find('.getContext').trigger('click');
    } else {
      $('.getContext').first().trigger('click');
      $('.question-jumbotron').show();
      $('.feedback-jumbotron').hide();
      $('form').find('input[type="submit"]').val("Submit").removeClass('next-page');
    }
  },
  'click .deleteContext': (e) => {
    let jqEl = $(e.target)
      , id = jqEl.data("id")
      , type = jqEl.data("type")
      , resource_id = Router.current().params.id
      , page = { id: id, type: type };

    if (type == "article") {
      Meteor.call('deleteArticle', id);
      Meteor.call('deletePageFromResource', resource_id, page);
    } else if (type == "question") {
    Meteor.call('deleteQuestion', id);
      Meteor.call('deletePageFromResource', resource_id, page);
    } else if (type == "journaling") {
      Meteor.call('deleteJournaling', id);
      Meteor.call('deletePageFromResource', resource_id, page);
    }
  }

});
