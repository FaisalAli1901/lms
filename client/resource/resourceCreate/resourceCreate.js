import { Template } from 'meteor/templating';

Template.resourceCreate.onCreated(() => {
  Meteor.subscribe('users');
});

Template.resourceCreate.onRendered(() => {

  let editor = $('.coverPic').summernote({
    placeholder: '',
    disableResizeEditor: true,
    height: "550",
    toolbar: [
      ['insert', ['picture']]
    ],
    focus: false,
    callbacks: {
      onImageUpload: function (files, editor, welEditable) {
        $('.small-loader').removeClass('hidden');
        var self = this;
        var reader = new FileReader();
        reader.onload = function () {
          var imageFile = _.pick(files[0], 'name', 'type', 'size');   // Convert to EJSON-able object
          var imageData = reader.result;                              // Binary string version
          Meteor.call('saveImageFile', imageFile, imageData, function(err, res) {
            if (!err) {
              $(self).summernote('insertImage', res, slugify(imageFile.name));
              $('.small-loader').addClass('hidden');
              $('[name="coverPic"]').val(res);
              $('.note-toolbar').hide();
              $('#coverPic-error').remove();
            }
          });
        };
        reader.readAsBinaryString(new Blob([files[0]]));
      },
      onMediaDelete : function($target, editor, $editable) {
        $('.note-toolbar').show();
      }
    }
  });

  let descriptionInput = $('.description-input').summernote({
    placeholder: 'Description (between 80 and 90 characaters)',
    disableResizeEditor: true,
    height: "250",
    disableResizeEditor: false,
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['insert', ['picture', 'video']]
    ],
    focus: false,
    callbacks: {
      onChange: function (contents, $editable) {
        let target_el = event.target,
          note_editor = $(target_el).closest('.note-editor'),
          summernote_input = note_editor.siblings('.description-input');

        summernote_input.val(contents);
      },
      onImageUpload: function (files, editor, welEditable) {
        var self = this;
        var reader = new FileReader();
        reader.onload = function () {
          var imageFile = _.pick(files[0], 'name', 'type', 'size');   // Convert to EJSON-able object
          var imageData = reader.result;                              // Binary string version
          Meteor.call('saveImageFile', imageFile, imageData, function(err, res) {
            if (!err) {
              $(self).summernote('insertImage', res, slugify(imageFile.name));
            }
          });
        };
        reader.readAsBinaryString(new Blob([files[0]]));
      },
      onMediaDelete : function($target, editor, $editable) {
        $('.note-toolbar').show();
      }
    }
  });

  validateResourceForm();

  $('.tags').tagsinput({
    trimValue: true,
    maxTags: 5
  });

  $('[name="tags"]').change(function (e) {
    $('#resource_form').valid()
    if($(this).hasClass('error')) {
      $('.bootstrap-tagsinput > input').addClass('error');
    } else {
      $('.bootstrap-tagsinput > input').removeClass('error');
    }
  });

});

Template.resourceCreate.events({
  // Save a resource to server on publish
  'submit #resource_form': (e) => {

    e.preventDefault();
    let form = e.target;

    validateResourceForm();
    let isValid = $(form).valid();

    if (isValid) {
      let data = {
        program: form.program.value,
        title: form.title.value,
        description: form.description.value,
        tags: form.tags.value,
        time: form.time.value,
        coverPic: form.coverPic.value,
        createdAt: Date.now(),
        views: 0,
        stars: 3,
        author: Meteor.user().emails[0].address,
        like: [],
        bookmark: [],
        pages: []
      };
      Meteor.call('createResource', data, (error, result) => {
        if (error) {
          alert('Error');
        } else {
          Router.go(`/resource/${result}/add`);
        }
      });
    }
  },
  'click .toggle-toolbar': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  },
  'click .save-resource': (e) => {
    e.preventDefault();
    $("#resource_form").submit();
  }

});

function validateResourceForm() {
  $('#resource_form').validate({
    ignore: [],
    rules: {
      'title': {
        required: true
      }
    },
    messages: {
      'title': {
        required: ''
      }
    },
    invalidHandler: function(event, validator) {
      if(validator.errorMap.hasOwnProperty('tags')) {
        $('.bootstrap-tagsinput > input').addClass('error');
      } else {
        $('.bootstrap-tagsinput > input').removeClass('error');
      }
    }
  });
}