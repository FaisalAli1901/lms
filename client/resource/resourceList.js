Template.body.helpers({
  resourcesListCursor: function() {
    return Resource.list;
  }
});

Template.resourceList.onCreated(() => {
  Meteor.subscribe('resources');
  Meteor.subscribe('users');
});

Template.resourceList.onRendered(() => {
  localStorage.setItem("horizontalScrollValue", "0");
});


Template.resourceComponentList.created = function() {
  var self = this;
  self.limitBy = this.data.limitBy || 1;
  self.limit = new ReactiveVar(self.limitBy);
  self.autorun(function() {
    self.subscription = Meteor.subscribe(self.data.subscription, self.limit.get());
  });
};


Template.resourceComponentList.helpers({
  items: function() {
    // return this.cursor({ limit: Template.instance().limit.get() });
  },
  showLoadButton: function() {
    return Counts.get(Template.instance().data.subscription) > Template.instance().limit.get();
  },
  loading: function() {
    return Template.instance().subscription &&
      ! Template.instance().subscription.ready();
  }
});


Template.resourceComponentList.events({
  'click [data-load]': function(event, template) {
    event.preventDefault()
    template.limit.set(template.limit.get() + template.limitBy);
    let scrollValue = parseInt(localStorage.getItem("horizontalScrollValue"));
    localStorage.setItem("horizontalScrollValue", scrollValue + 370);
    $('html, body').animate({
      scrollLeft: scrollValue + 370
    }, 300);
console.log(scrollValue)
  },
  'click .prev-resources': function(event, template) {
    event.preventDefault()
    let scrollValue = parseInt(localStorage.getItem("horizontalScrollValue"));
    if(scrollValue > 0) {
      localStorage.setItem("horizontalScrollValue", scrollValue - 370);
    }
    $('html, body').animate({
      scrollLeft: scrollValue - 370
    }, 300);
    console.log(scrollValue)
  }

});
