import { Template } from 'meteor/templating';

Template.question.onCreated(function () {
  Meteor.subscribe('questions');
});

Template.question.helpers({
  alphabets() {
    return ['a', 'b', 'c', 'd', 'e'];
  },
  offset(i) {
    return i;
  }
});

Template.question.events({
  'submit .question_answer_form': (e) => {
    e.preventDefault();

    $('.question-jumbotron').hide();

    let form = e.target,
      question = Template.instance().data
    answer = "";

    if (question.multiple == "checkbox") {
      answer = $.map($('.options-container :checkbox[name=question]:checked'), function (n, i) {
        return n.value;
      }).join(',')
    } else {
      answer = form.question.value;
    }
    if (question.answer == answer) {
      $('.question-jumbotron').after("<div class='jumbotron feedback-jumbotron'><br><br><br><br><br><img src='/correct.png' class='correct center-block'><br><p class='feedback'>That’s correct! Keep it up!</p><br></div>");
    } else {
      $('.question-jumbotron').after("<div class='jumbotron feedback-jumbotron'><br><br><br><br><br><img src='/sad.png' class='wrong center-block'><br><p class='feedback'>That’s wrong! Please try again!</p><br></div>");
    }
    $('form')[0].reset();
    $('form').find('input[type="submit"]').val("Next").addClass('next-page');
  },
  'click .next-page': (e) => {
    e.preventDefault();
    $('.question-jumbotron').show();
    $('.feedback-jumbotron').hide();
    $('form').find('input[type="submit"]').val("Submit").removeClass('next-page');
    let itemContent = $('.item-content.active');
    if (itemContent.length > 0) {
      $(itemContent).closest('.item').next('.item').find('.getContext').trigger('click');
    } else {
      $('.getContext').first().trigger('click');
    }
  }
});
