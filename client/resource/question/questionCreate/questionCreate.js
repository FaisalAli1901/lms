import { Template } from 'meteor/templating';

Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
  return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

Template.questionCreate.onRendered(() => {
  $('.summernote').summernote({
    disableResizeEditor: false,
    focus: true,
    callbacks: {
      onChange: function (contents, $editable) {
        let target_el = event.target,
          note_editor = $(target_el).closest('.note-editor'),
          summernote_input = note_editor.siblings('.summernote.option'),
          options_array = ["a", "b", "c", "d", "e"],
          summernote_inputs = "";

        if ($('#tab_default_2.active').length) {
          summernote_inputs = $('#tab_default_2 .summernote');
        } else {
          summernote_inputs = $(' .tab-pane.active.multiple .summernote');
        }
        summernote_input.val($(target_el).html());
        options = [];
        $.each(summernote_inputs, function (index, value) {
          let option = options_array[index],
            content = $(value).val(),
            image = $(value).siblings('.media-group-buttons').find('.img-preview').attr("src"),
            video = $(value).siblings('.media-group-buttons').find('iframe').attr("src");

          data = {
            "option": option,
            "content": content,
            "image": image,
            "video": video
          }

          options.push(data);
        });
        $('[name="options"]').val(JSON.stringify(options));
      },
      onImageUpload: function (files, editor, welEditable) {
        // sendFile(files[0],editor,welEditable);
        for (var i = files.length - 1; i >= 0; i--) {
          sendFile(files[i], this);
        }

        function sendFile(file, el) {
          data = new FormData();
          data.append("file", file);
          FS.Utility.eachFile(event, function (file) {
            Images.insert(file, function (err, fileObj) {
              if (err) {
                console.log("error in inserting the image");
              } else {
                var imagesURL = "/cfs/files/images/" + fileObj._id;
                setTimeout(() => {
                  $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                }, 1000);

              }
            });
          });
        }
      }
    }
  });

  validateQuestionForm();


  let questionDescriptionInput = $('.question-description-input').summernote({
    placeholder: 'Description',
    disableResizeEditor: true,
    height: "250",
    disableResizeEditor: false,
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['insert', ['picture', 'video']]
    ],
    focus: false,
    callbacks: {
      onChange: function (contents, $editable) {
        let target_el = event.target,
          note_editor = $(target_el).closest('.note-editor'),
          summernote_input = note_editor.siblings('.description-input');

        summernote_input.val(contents);
      },
      onImageUpload: function (files, editor, welEditable) {
        var self = this;
        var reader = new FileReader();
        reader.onload = function () {
          var imageFile = _.pick(files[0], 'name', 'type', 'size');   // Convert to EJSON-able object
          var imageData = reader.result;                              // Binary string version
          Meteor.call('saveImageFile', imageFile, imageData, function(err, res) {
            if (!err) {
              $(self).summernote('insertImage', res, slugify(imageFile.name));
            }
          });
        };
        reader.readAsBinaryString(new Blob([files[0]]));
      },
      onMediaDelete : function($target, editor, $editable) {
        $('.note-toolbar').show();
      }
    }
  });

  $('.question-tags').tagsinput({
    trimValue: true,
    maxTags: 5
  });

  $('[name="tags"]').change(function (e) {
    $('.question_form').valid();
    if ($(this).hasClass('error')) {

      $('.left-question-jumbotron .bootstrap-tagsinput > input').addClass('error');
    } else {
      $('.left-question-jumbotron .bootstrap-tagsinput > input').removeClass('error');
    }
  });

});

Template.questionCreate.helpers({
  get_question_context() {
    let context = Session.get('context');
    if (context && context.split("-")[0] == "question") {
      setTimeout(() => {
        $('.question-template [name="tags"]').tagsinput('add', $('.question-template [name="tags"]').val())
      }, 500);
      return Questions.findOne({
        _id: context.split("-")[1]
      });
    } else {
      return new ReactiveVar({
        stem: '',
        tags: '',
        description: '',
        inputs: '',
        multiple: '',
        options: []
      });
    }
  }
});


Template.questionCreate.events({
  'change input[type="file"]': (e) => {
    e.preventDefault();
    var reader = new FileReader();
    let files = e.target.files;
    let imagePreview = $(e.currentTarget).closest('.media-group-buttons').find('.img-preview');
    reader.onload = function () {
      var imageFile = _.pick(files[0], 'name', 'type', 'size');   // Convert to EJSON-able object
      var imageData = reader.result;                              // Binary string version
      Meteor.call('saveImageFile', imageFile, imageData, function(err, res) {
        if (!err) {
          $(imagePreview).attr("src", res).removeClass('hidden')
          $(imagePreview).closest('.media-group-buttons').find('.btn-sm').remove();
        }
      });
    };
    reader.readAsBinaryString(new Blob([files[0]]));
  },
  'click .toggle-toolbar': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  },
  'click .remove-option': (e) => {
    var jqEl = $(e.currentTarget);
    jqEl.closest('.col-sm-12').fadeOut('300', () => {
      jqEl.closest('.col-sm-12').remove();
      if ($('.multiple.active.tab-pane .options-row > .col-sm-12').length <= 4) {
        $('.multiple.active.tab-pane .add-another-option').css('display', 'block');
      } else {
        $('.multiple.active.tab-pane .add-another-option').css('display', 'remove');
      }
      let options_array = ["a", "b", "c", "d", "e"],
        summernote_inputs = $('.multiple.active.tab-pane .summernote');
      options = [];
      $.each(summernote_inputs, function (index, value) {
        let option = options_array[index],
          content = $(value).val(),
          image = $(value).siblings('.media-group-buttons').find('.img-preview').attr("src"),
          video = $(value).siblings('.media-group-buttons').find('iframe').attr("src");

        data = {
          "option": option,
          "content": content,
          "image": image,
          "video": video
        }

        options.push(data);
      });
      $('[name="options"]').val(JSON.stringify(options));
    });
  },
  'click .add-another-option': (e) => {
    var jqEl = $(e.currentTarget),
      type = jqEl.data('type'),
      date_string = new Date().getTime(),
      template = `<div class="col-sm-12 flex-box flex-vertical-center-align appended-option">
                      <div class="input-group col-sm-11 flex-box">
                        <div class="${type} ${type}-purple">
                          <input type="${type}" name="answer" id="${type}-${date_string}">
                          <label for="${type}-${date_string}"></label>
                        </div>
                        <div class="media-group-buttons">
                          <button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-picture"></span><input type="file" class="img_url" /></button>
                          <button type="button" class="btn btn-default btn-sm question-video-link"><span class="glyphicon glyphicon-facetime-video"></span></button>
                          <div class="img-preview-container">
                            <img class="img-preview img-thumbnail hidden" src="" alt="">
                          </div>
                          <div class="video-preview-container">
                          </div>
                        </div>
                        <input type="text" class="form-control option summernote">
                      </div>
                      <div class="col-sm-1">
                        <img class="remove-option" src="/trash.png" alt="trash icon">
                      </div>
                    </div>`;
    $('.tab-pane.active.multiple .options-row .clearfix').before(template);
    if ($('.multiple.active.tab-pane .options-row > .col-sm-12').length <= 4) {
      $('.multiple.active.tab-pane .add-another-option').css('display', 'block');
    } else {
      $('.multiple.active.tab-pane .add-another-option').css('display', 'none');
    }
    $('.summernote').summernote({
      disableResizeEditor: false,
      focus: true,
      callbacks: {
        onChange: function (contents, $editable) {
          let target_el = event.target,
            note_editor = $(target_el).closest('.note-editor'),
            summernote_input = note_editor.siblings('.summernote.option'),
            options_array = ["a", "b", "c", "d", "e"],
            summernote_inputs = "";

          if ($('#tab_default_2.active').length) {
            summernote_inputs = $('#tab_default_2 .summernote');
          } else {
            summernote_inputs = $(' .tab-pane.active.multiple .summernote');
          }
          summernote_input.val($(target_el).html());
          options = [];
          $.each(summernote_inputs, function (index, value) {
            let option = options_array[index],
              content = $(value).val(),
              image = $(value).siblings('.media-group-buttons').find('.img-preview').attr("src"),
              video = $(value).siblings('.media-group-buttons').find('iframe').attr("src");

            data = {
              "option": option,
              "content": content,
              "image": image,
              "video": video
            }
            options.push(data);
          });
          $('[name="options"]').val(JSON.stringify(options));
        },
        onImageUpload: function (files, editor, welEditable) {
          // sendFile(files[0],editor,welEditable);
          for (var i = files.length - 1; i >= 0; i--) {
            sendFile(files[i], this);
          }

          function sendFile(file, el) {
            data = new FormData();
            data.append("file", file);
            FS.Utility.eachFile(event, function (file) {
              Images.insert(file, function (err, fileObj) {
                if (err) {
                  console.log("error in inserting the image");
                } else {
                  var imagesURL = "/cfs/files/images/" + fileObj._id;
                  setTimeout(() => {
                    $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                  }, 1000);

                }
              });
            });
          }
        }
      }
    });
  },
  'submit .question_form': (e) => {
    e.preventDefault();
    let form = e.target;

    let isValid = $('.question_form').valid(),
      options_array = ["a", "b", "c", "d", "e"];

    if (isValid) {
      let data = {
        stem: form.stem.value,
        tags: form.tags.value,
        description: form.description.value,
        options: options,
        multiple: ($('.main-options-tab').hasClass('active') && $('.multiple.active.tab-pane').attr("id") == "tab_default_5") ? "checkbox" : "radio"
      };
      if ($('#tab_default_2.active').length > 0) {
        let radio_elements = $('#tab_default_2 :radio');
        let index = null;
        radio_elements.each((i, v) => {
          if ($(v).is(":checked")) {
            index = i;
          }
        });
        data.answer = options_array[index];
      } else if ($('.multiple.active.tab-pane').attr("id") == "tab_default_5") {
        let checkbox_elements = $('.multiple.active [name="answer"]:checkbox');
        indices = [];

        checkbox_elements.each((i, v) => {
          if ($(v).is(":checked")) {
            indices.push(i);
          }
        });
        answer_string = "";
        options_array.filter((v, i) => {
          if (indices.indexOf(i) > -1) {
            if (answer_string === "") {
              answer_string += v;
            } else {
              answer_string += "," + v;
            }
          }
        })
        data.answer = answer_string;
      } else if ($('.multiple.active.tab-pane').attr("id") == "tab_default_4") {
        let radio_elements = $('.multiple.active :radio');
        let index = null;
        radio_elements.each((i, v) => {
          if ($(v).is(":checked")) {
            index = i;
          }
        });
        data.answer = options_array[index];
      }

      let context = Session.get('context');
      if (context && context.split("-")[0] == "question") {
        Meteor.call('updateQuestion', context.split("-")[1], data, (error, result) => {
          if (error) {
            console.log('Error');
          }
        });
      } else {
        Meteor.call('createQuestion', data, (error, result) => {
          if (error) {
            console.log('Error');
          } else {
            let page = {
                type: "question",
                id: result
              },
              resource_id = Router.current().params.id;
            Meteor.call('addPagetoResource', resource_id, page, (err, result) => {
              if (result) {
                $('.question_form')[0].reset();
                $('.questionDescriptionEditable').html('');
                $('.appended-option').remove();
                $('.add-another-option').show();
                $('.tags').tagsinput('removeAll');
                $('.bootstrap-tagsinput > input').removeClass('error');
                $("[id$='-error']").remove();
                $('.error').removeClass('error');
                options = [];
                $('[name="options"]').val("");
                $('.note-editable').html('');
                $('.nav-tabs a[href="#article"]').tab('show');
                $('.img-preview').attr("src", "").addClass('hidden');
              }
            });
          }
        });

      }
    }
  },
	'click .question-video-link': (e, t) => {
    e.preventDefault();
    let el = $(e.currentTarget);
		el.addClass('clicked');
		Modal.show('questionVideoModal', el);
	}

});

Template.questionVideoModal.events({
	'click .btn-add': (e, t) => {
    e.preventDefault();
    let el = $(e.currentTarget),
      videoUrl = el.closest('.question-video').find('input').val().trim();

      var matches = videoUrl.match(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g);

      if (!matches) {
          return true;
      } else {
        let iFrameString = videoUrl.replace(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<iframe width="150px" height="100px" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>');
        $(iFrameString).insertAfter(document.querySelector('.clicked'));
        Modal.hide('questionVideoModal');
        $('.clicked').closest('.media-group-buttons').find('.btn-sm').remove();
        $('.question-video-link').removeClass('clicked');
      }
	}
});

function validateQuestionForm() {
  $('.question_form').validate({
    ignore: [],
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.prependTo(element.parent());
    },
    rules: {
      'stem': {
        required: true
      },
      'tags': {
        required: true
      },
      'instructions': {
        required: true
      },
      'answer': {
        required: true
      }
    },
    messages: {
      'stem': {
        required: ''
      },
      'tags': '',
      'instructions': '',
      'answer': 'Select option'
    },
    invalidHandler: function (event, validator) {
      if (validator.errorMap.hasOwnProperty('tags')) {
        $('.left-question-jumbotron .bootstrap-tagsinput > input').addClass('error');
      } else {
        $('.left-question-jumbotron .bootstrap-tagsinput > input').removeClass('error');
      }
    }
  });
}