import { Template } from 'meteor/templating';

Template.journaling.onCreated(function () {
  this.currentTab = new ReactiveVar("opts");
  Meteor.subscribe('journaling');
});

Template.journaling.helpers({
  isText: function () {
    if (this.inputs.indexOf("text") > -1) {
      return true;
    }
    return false;
  },
  opts: function () {
    var opts = {
      maxTime: 120,
      androidQuality: 0,
      videoDisplay: {
        width: "100%",
        height: "600"
      },
      classes: {
        recordBtn: 'video-capture-basic-record-btn',
        stopBtn: 'video-capture-basic-stop-btn'
      },
      onVideoRecorded: function (err, base64Data) {
        console.log('onVideoRecorded');
        let videoRecordButton = document.querySelector('.lm-video-capture-record-start-btn > img')
        videoRecordButton.src = "/button-retake.png";
        let previewVideo = document.querySelector('.lm-video-capture-record-preview-btn')
        previewVideo.style.display = "block";
        let stopVideo = document.querySelector('.lm-video-capture-record-stop-btn')
        stopVideo.style.display = "none";
      }
    };
    return opts;
  }
});

Template.journaling.events({
  'submit .journaling_answer_form': (e) => {
    e.preventDefault();

    $('.journaling-jumbotron').hide();
    $('.journaling-jumbotron').after("<div class='jumbotron feedback-jumbotron'><br><br><br><br><br><img src='/correct.png' class='correct center-block'><br><p class='feedback'>Thank you! Your response is recorded!</p><br></div>");
  },
  'click .nav-pills li': function (event, template) {
    var currentTab = $(event.target).closest("li");

    currentTab.addClass("active");
    $(".nav-pills li").not(currentTab).removeClass("active");

    template.currentTab.set(currentTab.data("template"));
  },
  'click .start-recording': function (e) {
    e.preventDefault();
    var jqEl = $(event.target);

    jqEl.addClass("hidden");
    $('.journaling-video-jumbotron .panel-heading').addClass("hidden");
    $('.video-record-container').removeClass("hidden");
  },
  'click .lm-video-capture-record-cancel-btn': function (e) {
    e.preventDefault();
    var jqEl = $(event.target);

    $('.journaling-video-jumbotron .panel-heading').removeClass("hidden");
    $('.start-recording').removeClass("hidden");
    $('.video-record-container').addClass("hidden");
    document.getElementById('play-video').src = "";
    let previewVideo = document.querySelector('.lm-video-capture-record-preview-btn');
    previewVideo.style.display = "none";
    let videoRecordButton = document.querySelector('.lm-video-capture-record-start-btn > img')
    videoRecordButton.src = "/button-record.png";
  },
  'click .lm-video-capture-record-start-btn': function (e) {
    e.preventDefault();

    let stopVideo = document.querySelector('.lm-video-capture-record-stop-btn');
    stopVideo.style.display = "block";
  },
  'click .lm-video-capture-record-preview-btn': function (e) {
    e.preventDefault();
    document.getElementById('play-video').play();
  }

});