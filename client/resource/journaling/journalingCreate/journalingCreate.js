import { Template } from 'meteor/templating';

Template.journalingCreate.onRendered(function () {
  $('.summernote').summernote({
    placeholder: 'start typing your question',
    disableResizeEditor: true,
    focus: false,
    airmode: true,
    popover: {
      image: [
        ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
      link: [
        ['link', ['linkDialogShow', 'unlink']]
      ],
      air: [
        ['color', ['color']],
        ['font', ['bold', 'italic', 'underline', 'clear', 'fontname', "fontsize"]],
        ['para', ['style', 'ol', 'ul', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']]
      ]
    },
    callbacks: {
      onChange: function (contents, $editable) {
        let target_el = event.target,
          note_editor = $(target_el).closest('.note-editor'),
          summernote_input = note_editor.siblings('.summernote');

        summernote_input.val($(target_el).html());

      },
      onImageUpload: function (files, editor, welEditable) {
        // sendFile(files[0],editor,welEditable);
        for (var i = files.length - 1; i >= 0; i--) {
          sendFile(files[i], this);
        }

        function sendFile(file, el) {
          data = new FormData();
          data.append("file", file);
          FS.Utility.eachFile(event, function (file) {
            Images.insert(file, function (err, fileObj) {
              if (err) {
                console.log("error in inserting the image");
              } else {
                var imagesURL = "/cfs/files/images/" + fileObj._id;
                setTimeout(() => {
                  $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                }, 1000);

              }
            });
          });
        }
      }
    }
  });

  validateJournalingForm();

  let journalingDescriptionInput = $('.journaling-description-input').summernote({
    placeholder: 'Description',
    disableResizeEditor: true,
    height: "250",
    disableResizeEditor: false,
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['insert', ['picture', 'video']]
    ],
    focus: false,
    callbacks: {
      onChange: function (contents, $editable) {
        let target_el = event.target,
          note_editor = $(target_el).closest('.note-editor'),
          summernote_input = note_editor.siblings('.description-input');

        summernote_input.val(contents);
      },
      onImageUpload: function (files, editor, welEditable) {
        var self = this;
        var reader = new FileReader();
        reader.onload = function () {
          var imageFile = _.pick(files[0], 'name', 'type', 'size');   // Convert to EJSON-able object
          var imageData = reader.result;                              // Binary string version
          Meteor.call('saveImageFile', imageFile, imageData, function(err, res) {
            if (!err) {
              $(self).summernote('insertImage', res, slugify(imageFile.name));
            }
          });
        };
        reader.readAsBinaryString(new Blob([files[0]]));
      },
      onMediaDelete : function($target, editor, $editable) {
        $('.note-toolbar').show();
      }
    }
  });


  $('.journaling-tags').tagsinput({
    trimValue: true,
    maxTags: 5
  });

  $('[name="tags"]').change(function (e) {
    $('.journaling_form').valid()
    if ($(this).hasClass('error')) {
      $('.left-journaling-jumbotron .bootstrap-tagsinput > input').addClass('error');
    } else {
      $('.left-journaling-jumbotron .bootstrap-tagsinput > input').removeClass('error');
    }
  });

});

Template.journalingCreate.onCreated(function () {
  this.currentTab = new ReactiveVar("opts");
  // this.currentidForContent = new ReactiveVar( "" );
});

Template.journalingCreate.helpers({

  opts: function () {
    var opts = {
      maxTime: 120,
      androidQuality: 0,
      videoDisplay: {
        width: 480,
        height: 360
      },
      classes: {
        recordBtn: 'video-capture-basic-record-btn',
        stopBtn: 'video-capture-basic-stop-btn'
      },
      onVideoRecorded: function (err, base64Data) {
        console.log('onVideoRecorded');
      }
    };
    return opts;
  }
});


Template.journalingCreate.helpers({
  get_journaling_context() {
    let context = Session.get('context');
    if (context && context.split("-")[0] == "journaling") {
      return Journaling.findOne({
        _id: context.split("-")[1]
      });
    } else {
      return new ReactiveVar({
        stem: '',
        instructions: '',
        tags: '',
        inputs: ''
      });
    }
  }
});

Template.journalingCreate.events({
  'click .toggle-toolbar': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  },
  'submit .journaling_form': (e) => {
    e.preventDefault();
    let form = e.target;

    // validateJournalingForm();
    let isValid = $('.journaling_form').valid();

    if (isValid) {
      let data = {
        stem: form.stem.value,
        description: form.description.value,
        tags: form.tags.value,
        inputs: form.inputs.value
      };

      let context = Session.get('context');
      if (context && context.split("-")[0] == "journaling") {
        Meteor.call('updateJournaling', context.split("-")[1], data, (error, result) => {
          if (error) {
            console.log('Error');
          }
        });
      } else {
        Meteor.call('createJournaling', data, (error, result) => {
          if (error) {
            console.log('Error');
          } else {
            let page = {
                type: "journaling",
                id: result
              },
              resource_id = Router.current().params.id;
            Meteor.call('addPagetoResource', resource_id, page, (err, result) => {
              if (result) {
                $('.journaling_form')[0].reset();
                $('.journalingDescriptionEditable').html('');
                $('.note-editable').html('');
                $('.tags').tagsinput('removeAll');
                $("[id$='-error']").remove();
                $('.error').removeClass('error');
                $('.nav-tabs a[href="#article"]').tab('show');
              }
            });
          }
        });
      }
    }
  }

});


function validateJournalingForm() {
  $('.journaling_form').validate({
    ignore: [],
    rules: {
      'stem': {
        required: true
      },
      'tags': {
        required: true
      }
    },
    messages: {
      'stem': {
        required: ''
      },
      'tags': ''
    },
    invalidHandler: function (event, validator) {
      if (validator.errorMap.hasOwnProperty('tags')) {
        $('.left-journaling-jumbotron .bootstrap-tagsinput > input').addClass('error');
      } else {
        $('.left-journaling-jumbotron .bootstrap-tagsinput > input').removeClass('error');
      }
    }
  });
}