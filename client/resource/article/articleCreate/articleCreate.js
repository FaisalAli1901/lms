import { Template } from 'meteor/templating';

Template.articleCreate.onRendered(() => {

  $('a[title]').tooltip({
    "placement": 'bottom'
  });

  Template.articleCreate.__helpers[" initArticleSummernotes"]();

  $('.article_form').validate({
    ignore: [],
    rules: {
      'left_html': {
        required: true
      },
      'right_html': {
        required: true
      }
    },
    messages: {
      'left_html': {
        required: "Enter text image or video"
      },
      'right_html': {
        required: "Enter text image or video"
      }
    }
  });
});

Template.articleCreate.helpers({
  get_article_context() {
    let context = Session.get('context');
    let self = this;
    if (context && context.split("-")[0] == "article") {
      let left_html_content = $('[name="left_html"]').val();
      let right_html_content = $('[name="right_html"]').val();
      // setTimeout(() => {
      Template.articleCreate.__helpers[" initArticleSummernotes"]();
      // }, 500);
      return Articles.findOne({
        _id: context.split("-")[1]
      });
    } else {
      return new ReactiveVar({
        left_html: '',
        right_html: ''
      });
    }
  },
  initArticleSummernotes() {
    $('.summernote-left-article').summernote('destroy');
    $('.summernote-center-article').summernote('destroy');
    $('.summernote-right-article').summernote('destroy');
    let left_article_editor = $('.summernote-left-article').summernote({
      placeholder: 'start typing',
      disableResizeEditor: false,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['table', 'link', 'picture', 'video']]
      ],
      focus: true,
      callbacks: {
        onChange: function (contents, $editable) {
          let target_el = event.target,
            note_editor = $(target_el).closest('.note-editor'),
            summernote_input = note_editor.siblings('.summernote-left-article');

          summernote_input.val(contents);
        },
        onImageUpload: function (files, editor, welEditable) {
          for (var i = files.length - 1; i >= 0; i--) {
            sendFile(files[i], this);
          }

          function sendFile(file, el) {
            data = new FormData();
            data.append("file", file);
            FS.Utility.eachFile(event, function (file) {
              Images.insert(file, function (err, fileObj) {
                if (err) {
                  console.log("error in inserting the image");
                } else {
                  var imagesURL = "/cfs/files/images/" + fileObj._id;
                  setTimeout(() => {
                    $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                    $('[name="coverPic"]').val(location.origin + imagesURL);
                  }, 1000);
                }
              });
            });
          }
        }
      }
    });
    let center_article_editor = $('.summernote-center-article').summernote({
      placeholder: 'start typing',
      disableResizeEditor: false,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['table', 'link', 'picture', 'video']]
      ],
      focus: true,
      callbacks: {
        onChange: function (contents, $editable) {
          let target_el = event.target,
            note_editor = $(target_el).closest('.note-editor'),
            summernote_input = note_editor.siblings('.summernote-left-article');

          summernote_input.val(contents);
        },
        onImageUpload: function (files, editor, welEditable) {
          for (var i = files.length - 1; i >= 0; i--) {
            sendFile(files[i], this);
          }

          function sendFile(file, el) {
            data = new FormData();
            data.append("file", file);
            FS.Utility.eachFile(event, function (file) {
              Images.insert(file, function (err, fileObj) {
                if (err) {
                  console.log("error in inserting the image");
                } else {
                  var imagesURL = "/cfs/files/images/" + fileObj._id;
                  setTimeout(() => {
                    $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                    $('[name="coverPic"]').val(location.origin + imagesURL);
                  }, 1000);
                }
              });
            });
          }
        }
      }
    });
    let right_article_editor = $('.summernote-right-article').summernote({
      placeholder: 'start typing',
      disableResizeEditor: false,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['table', 'link', 'picture', 'video']]
      ],
      focus: false,
      callbacks: {
        onChange: function (contents, $editable) {
          let target_el = event.target,
            note_editor = $(target_el).closest('.note-editor'),
            summernote_input = note_editor.siblings('.summernote-right-article');

          summernote_input.val(contents);
        },
        onImageUpload: function (files, editor, welEditable) {
          for (var i = files.length - 1; i >= 0; i--) {
            sendFile(files[i], this);
          }

          function sendFile(file, el) {
            data = new FormData();
            data.append("file", file);
            FS.Utility.eachFile(event, function (file) {
              Images.insert(file, function (err, fileObj) {
                if (err) {
                  console.log("error in inserting the image");
                } else {
                  var imagesURL = "/cfs/files/images/" + fileObj._id;

                  setTimeout(() => {
                    $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                    $('[name="coverPic"]').val(location.origin + imagesURL);
                  }, 1000);
                }
              });
            });
          }
        }
      }
    });
  }

});

Template.articleCreate.events({
  'click .toggle-toolbar': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  },
  'change [name="one_column"]': (e) => {
    e.preventDefault();

    $('.content-container').toggleClass('hidden');
  },

  // Save articles to server on publish
  'submit .article_form': (e) => {
    e.preventDefault();
    let form = e.target,
      is_two_column = $('[name="one_column"]').is(":checked"),
      data = {};

    let isValid = $('.article_form').valid();

    if (isValid) {
      if (is_two_column) {
        data = {
          one_column: false,
          left_html: form.left_html.value,
          right_html: form.right_html.value
        };
      } else {
        data = {
          one_column: true,
          center_html: form.center_html.value,
        };
      }

      let context = Session.get('context');
      if (context && context.split("-")[0] == "article") {
        Meteor.call('updateArticle', context.split("-")[1], data, (error, result) => {
          if (error) {
            console.log('Error');
          }
        });
      } else {
        Meteor.call('createArticle', data, (error, result) => {
          if (error) {
            console.log('Error');
          } else {
            let page = {
                type: "article",
                id: result
              },
              resource_id = Router.current().params.id;
            Meteor.call('addPagetoResource', resource_id, page, (err, result) => {
              if (result) {
                $('.article_form')[0].reset();
                $('.note-editable').html('');
                $('.nav-tabs a[href="#article"]').tab('show');
              }
            });
          }
        });
      }

    }
  }
});