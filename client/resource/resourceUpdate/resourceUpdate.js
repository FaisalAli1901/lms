import { Template } from 'meteor/templating';

Template.resourceUpdate.onCreated(() => {
  Meteor.subscribe('resources');
  Meteor.subscribe('users');
});


Template.resourceUpdate.onRendered(() => {
  $('a[title]').tooltip({"placement": 'bottom'});

  $(document).on('shown.bs.tab', '#pageTab a[data-toggle="tab"]', function (e) {
    $('.save-resource').removeClass('disabled');
  });
});

/* Registering handlebars math functions */
Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
  lvalue = parseFloat(lvalue);
  rvalue = parseFloat(rvalue);

  return {
      "+": lvalue + rvalue,
      "-": lvalue - rvalue,
      "*": lvalue * rvalue,
      "/": lvalue / rvalue,
      "%": lvalue % rvalue
  }[operator];
});


Template.resourceUpdate.helpers({
  getResource() {
    let resource_id = Router.current().params.id;

    if (resource_id) {
      return Resource.findOne({ _id: resource_id });
    }
  }
});

Template.resourceUpdate.events({
  'click #show-pages': (e) => {
    e.preventDefault();
    $('.toc-jumbotron').toggleClass('hidden');
  },
  // Save a resource to server on publish
  'submit #resource_form': (e) => {
    e.preventDefault();
    let form = e.target;

    let isValid = $(form).valid();

    if (isValid) {
      let data = {
        title: form.title.value,
        subtitle: form.subtitle.value,
        tags: form.tags.value,
        coverPic: form.coverPic.value
      };

      Meteor.call('createResource', data);
      location.href= "/";
    }
  },
  'click .save-resource': (e) => {
    $(e.currentTarget).addClass('disabled');
  },
  'click .new-page-button': (e) => {
    e.preventDefault();
    if($("#pageTab").find(".active a").data("original-title") == "Article") {
      $(".article_form").submit();
    }
    else if($("#pageTab").find(".active a").data("original-title") == "Question") {
      $(".question_form").submit();
    }
    else if($("#pageTab").find(".active a").data("original-title") == "Journaling") {
      $(".journaling_form").submit();
    }
    else if ($("#pageTab").find(".active a").data("original-title") == "Bot") {
      $(".bot_form").submit();
    }
  },
  'click .getContext': (e) => {
    e.preventDefault();

    let jqEl = $(e.target)
      , id = jqEl.data("id")
      , type = jqEl.data("type")
      ;

    $('.current-page-number').html($(e.target).closest('.item').prevAll('.item').length + 1);
    $('.item-content').removeClass('active');
    $(e.target).closest('.item-content').addClass('active');
    $('.nav-tabs a[href="#'+ type +'"]').tab('show');
    $('.question-jumbotron').show();
    $('.feedback-jumbotron').hide();
    $('form').find('input[type="submit"]').val("Submit").removeClass('next-page');
  },
  'click .editContext': (e) => {
    e.preventDefault();

    let jqEl = $(e.currentTarget)
      , id = jqEl.data("id")
      , type = jqEl.data("type")
      , resource_id = Router.current().params.id
      ;

    Session.set('context', `${type}-${id}`);
  },
  'click .deleteContext': (e) => {
    let jqEl = $(e.target)
      , id = jqEl.data("id")
      , type = jqEl.data("type")
      , resource_id = Router.current().params.id
      , page = { id: id, type: type };

    if (type == "article") {
      Meteor.call('deleteArticle', id);
      Meteor.call('deletePageFromResource', resource_id, page);
    } else if (type == "question") {
      Meteor.call('deleteQuestion', id);
      Meteor.call('deletePageFromResource', resource_id, page);
    } else if (type == "journaling") {
      Meteor.call('deleteJournaling', id);
      Meteor.call('deletePageFromResource', resource_id, page);
    }
  }

});