import { Template } from 'meteor/templating';

Template.bot.onDestroyed(() => {
  Session.set('botStates', "")
  Session.set('conversation', "")
})

Template.bot.onCreated(() => {
  Meteor.subscribe('BotConversation');
  Meteor.subscribe('currentTemplate');
});

let startConv

Template.bot.helpers({
  getResponse() {
    let botStates = Session.get('botStates')
    console.log(botStates);
    Meteor.subscribe('currentTemplate');
    let currentId = CurrentTemplate.findOne().id

    if (!botStates) {
      let temp = BotConversation.findOne({
        _id: currentId
      })
      console.log(temp);
      let response = temp.states.start.response
      startConv = response
      return response
    } else {
      let temp = BotConversation.findOne({
        _id: currentId
      })
      console.log(temp);
      let response = temp.states[botStates].response
      startConv = response
      return response
    }
  },
  getTemplate() {
    let conversation = Session.get('conversation')

    if (conversation) {
      console.log(conversation);
      return conversation
    }
  }
});

Template.bot.events({
  'click #talk': (e, t) => {
    e.preventDefault();
    // Meteor.subscribe('BotState');
    Meteor.subscribe('currentTemplate');
    Meteor.subscribe('BotConversation');

    let userResponse = t.find('#userResponse').value
    let currentId = CurrentTemplate.findOne().id
    let botStates = Session.get('botStates')
    t.find('#userResponse').value = ''

    if (botStates === undefined) {
      Meteor.call('getConversation', currentId, userResponse, 'start', (error, result) => {
        if (error) {
          console.log('error', error);
        }
        if (result) {
          let conversation = []
          Session.set('botStates', result)
          let botCon = {
            temp: 'responseBot',
            data: startConv
          }
          let conv = {
            temp: 'responseUser',
            data: userResponse
          }
          conversation.push(botCon)
          conversation.push(conv)
          Session.set('conversation', conversation)
        }
      });
    } else {
      Meteor.call('getConversation', currentId, userResponse, botStates, (error, result) => {
        if (error) {
          console.log('error', error);
        }
        if (result) {
          let conversation = Session.get('conversation')
          Session.set('botStates', result)
          let botCon = {
            temp: 'responseBot',
            data: startConv
          }
          let conv = {
            temp: 'responseUser',
            data: userResponse
          }
          conversation.push(botCon)
          conversation.push(conv)
          Session.set('conversation', conversation)
        }
      });
    }
  },
  'click #reset': (e, t) => {
    e.preventDefault();
    Session.set('botStates', '')
  }
});