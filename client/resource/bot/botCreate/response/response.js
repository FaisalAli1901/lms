import { Template } from 'meteor/templating';
import FetchData from '../fetchData/fetchData';

Template.botStartView.onCreated(() => {
  Meteor.subscribe('BotConversation');
});
Template.botResponseView.onCreated(() => {
  Meteor.subscribe('BotConversation');
});
Template.userResponse.onCreated(() => {
  Meteor.subscribe('BotConversation');
});
Template.userstart.onCreated(() => {
  Meteor.subscribe('BotConversation');
});

Template.botStartView.helpers({
  getResponse() {
    let botId = Session.get("botId")
    if (botId) {
      let temp = BotConversation.findOne({
        _id: botId
      });
      if (temp.states) {
        let response = temp.states.start.response
        return response;
      }
    }
  },
});

Template.botResponseView.helpers({
  getResponse(index) {
    let botId = Session.get("botId")
    let temp = BotConversation.findOne({
      _id: botId
    })

    if (temp.states) {
      let states = temp.states
      let data = new FetchData(states)
      let fetchData = data.statesArray
      if (fetchData) {
        return fetchData[index]
      }
    }
  },
});

Template.userResponse.helpers({
  getUserResponse(index) {
    let botId = Session.get("botId")
    let temp = BotConversation.findOne({
      _id: botId
    })
    if (temp.states) {
      let states = temp.states
      let data = new FetchData(states)
      let fetchData = data.statesArray
      if (fetchData) {
        return fetchData[index]
      }
    }
  }
});

Template.userResponse.events({
  'mouseover .tag': (e, t) => {
    e.preventDefault();
    let botId = Session.get("botId"),
        temp = BotConversation.findOne({
                _id: botId
              }),
        state = $(e.currentTarget).data("states"),
        tag = $(e.currentTarget).html().trim();

    $(e.currentTarget).attr('data-content', temp.states[state].response);
    $(e.currentTarget).popover({
      "placement": 'bottom',
      "trigger": 'hover',
      "html": 'true'
    });
    $(e.currentTarget).popover('show');
  }
});

Template.botStart.events({
  'click .bot-says': (e, t) => {
    e.preventDefault();
    Modal.show('botStartModel')
  },
});

Template.botResponse.events({
  'click .add-bot-response': (e, t) => {
    e.preventDefault();
    Modal.show('botResponseModel')
  }
});

Template.userstart.events({
  'click .add-user-response': (e, t) => {
    e.preventDefault();
    Modal.show('userStartModel')
  }
});