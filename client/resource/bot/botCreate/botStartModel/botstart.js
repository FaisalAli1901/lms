import { Template } from 'meteor/templating';

var botSummernote

Template.botStartModel.onRendered(() => {
  botSummernote = $('.summernote-right-botStart').summernote({
    placeholder: 'Type bot message here...',
    disableResizeEditor: false,
    height: "150",
    focus: false,
    toolbar: [
      ['insert', ['picture', 'video']]
    ],
    callbacks: {
      onImageUpload: function (files, editor, welEditable) {
        for (var i = files.length - 1; i >= 0; i--) {
          sendFile(files[i], this);
        }

        function sendFile(file, el) {
          data = new FormData();
          data.append("file", file);
          FS.Utility.eachFile(event, function (file) {
            Images.insert(file, function (err, fileObj) {
              if (err) {
                console.log("error in inserting the image");
              } else {
                var imagesURL = "/cfs/files/images/" + fileObj._id;

                setTimeout(() => {
                  $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                  $('[name="coverPic"]').val(location.origin + imagesURL);
                }, 1000);
              }
            });
          });
        }
      }
    }
  });
});

Template.botStartModel.events({
  'click .createConv': (e, t) => {
    e.preventDefault();
    let botStart = botSummernote.summernote('code');

    Meteor.call('createConversation', botStart, (error, result) => {
      if (error) {
        console.log(error);
      } else {
        let botId = result;
        let response = []
        Session.set('botId', botId)
        response.push('botStartView')
        Session.set('response', response)
        Session.set('botStart', false)
      }
    });
    Session.set('currentTemp', 'userstart')
    Modal.hide('botStartModel')
  },
  'click .close': (e) => {
    e.preventDefault();
    Modal.hide('botStartModel')
  },
  'click .float-plus': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-enter-active slide-leave-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  }
});
