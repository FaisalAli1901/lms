import { Template } from 'meteor/templating';

var botResSummernote
var botResponse = {}
var userTags = []

Template.botResponseModel.onRendered(() => {
  $('.userTag').first().trigger('click')
});

Template.botResponseModel.onDestroyed(() => {
  Session.set('currentTag', "")
})

Template.botResponseModel.helpers({
  getUserResponse() {
    let id = Session.get('botId')
    let userResponse = Session.get('tags')
    let userResponded = Session.get('userResponded')

    if (userResponded) {
      if (id) {
        let temp = BotConversation.findOne({
          _id: id
        });
        let fetchTag = Session.get('userTag')
        let temptags = temp.states[fetchTag[0]].userStates
        let tags = Object.keys(temptags)
        userTags = tags
        return tags
      }
    } else {
      if (id) {
        let temp = BotConversation.findOne({
          _id: id
        });
        let temptags = temp.states.start.userStates
        let tags = Object.keys(temptags)
        userTags = tags
        return tags
      }
    }
  },
  getTagName() {
    let tagName = Session.get('currentTag')
    if (tagName) {
      return tagName
    }
  }
});

Template.botResponseModel.events({
  'click .userTag': (e) => {
    e.preventDefault();

    let jqEl = $(e.currentTarget),
        tag = jqEl.attr("id"),
        plusIcon = $('.float-plus'),
        summernoteInitialized = $('.summernote-right-botresponse').data("summernoteInitialized");

      $('.userTag').removeClass('active');
      jqEl.addClass('active');

    Session.set('currentTag', tag)
    if(summernoteInitialized) {
      botResSummernote.summernote('reset');
    } else {
      botResSummernote = $('.summernote-right-botresponse').summernote({
        placeholder: 'Add your Response',
        disableResizeEditor: true,
        height: "200",
        focus: true,
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['insert', ['link', 'picture', 'video']]
        ]
      });
    }
  },
  'click .float-plus': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-enter-active slide-leave-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  },
  'click .save': (e, t) => {
    e.preventDefault();

    let tagName = e.target.id;
    let botRespond = botResSummernote.summernote('code');

    botResponse[tagName] = {
      response: botRespond
    }
    botResSummernote.summernote('reset');
  },
  'click .addToServer': (e, t) => {
    e.preventDefault();
    let responses = Session.get('response')
    let id = Session.get('botId');

    let tags = Session.get('tags')

    Meteor.call('botRespondOnUser', id, tags, botResponse);

    Session.set('botResponse', true)
    Session.set('userTag', userTags)
    responses.push('botResponseView')
    Session.set('response', responses)
    Session.set('currentTag', '')
    Session.set('userResponded', false)
    Session.set('currentTemp', 'userstart')
    Modal.hide('botResponseModel')
  },
  'click .close': (e) => {
    e.preventDefault();
    Modal.hide('botResponseModel')
  },
});
