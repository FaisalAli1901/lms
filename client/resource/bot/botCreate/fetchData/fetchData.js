export default class FetchData {
    constructor(states) {
        this.statesArray = []
        this.states = states
        this.looptoAdd('start')
    }
    looptoAdd(root){
        let addedState = []
        if (addedState.indexOf(root) === -1) {
            addedState.push(root)

            if (root === 'start') {
                if (this.statesArray.indexOf('start') === -1) {
                    this.statesArray.push(["start"])
                }
            }
            let obj = this.states[root]
            if (obj && Object.keys(obj).length !== 0) {
                let keys = Object.keys(obj)
                if (keys) {
                    if (keys.indexOf("userStates") >= 0) {
                        let userStates = Object.keys(this.states[root].userStates)
        
                        this.statesArray.push(userStates)
                        this.statesArray.push(userStates)
                        if (userStates.length > 0) {
                            this.looptoAdd(userStates[0])
                        } else {
                            console.log("did not find further states")
                        }
                    }
                }  
            }
        }
    }
}