import { Template } from 'meteor/templating';

Template.botCreate.onRendered(() => {
  let bot_img_upload = $('.summernote-right-bot').summernote({
    placeholder: '',
    disableResizeEditor: true,
    height: "550",
    toolbar: [
      ['insert', ['picture', 'video']]
    ],
    focus: false,
    callbacks: {
      onImageUpload: function (files, editor, welEditable) {
        for (var i = files.length - 1; i >= 0; i--) {
          sendFile(files[i], this);
        }

        function sendFile(file, el) {
          data = new FormData();
          data.append("file", file);
          FS.Utility.eachFile(event, function (file) {
            Images.insert(file, function (err, fileObj) {
              if (err) {
                console.log("error in inserting the image");
              } else {
                var imagesURL = "/cfs/files/images/" + fileObj._id;
                setTimeout(() => {
                  $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                  $('[name="coverPic"]').val(location.origin + imagesURL);
                }, 2000);
                $('.note-toolbar').hide();
                $('#coverPic-error').remove();
              }
            });
          });
        }
      },
      onMediaDelete: function ($target, editor, $editable) {
        $('.note-toolbar').show();
      }
    }
  });
});

Template.botCreate.onDestroyed(() => {
  Session.set('botID', "")
  Session.set('tags', "")
  Session.set('currentTag', "")
  Session.set('response', "")
  Session.set('botStart', '')
  Session.set('botResponse', false)
  Session.get('currentTemp', 'botStart')
})

Template.botCreate.helpers({
  getResponses() {
    let responses = Session.get('response');
    if (responses && responses.length > 0) {
      return responses
    }
  },
  getTemplate() {
    let currentTemp = Session.get('currentTemp')
    if (currentTemp == "botStart" || currentTemp == undefined) {
      return "botStart"
    } else {
      return currentTemp
    }
  }
});

Template.botCreate.events({
  'submit .bot_form': (e) => {
    e.preventDefault();
    console.log('called');
    let botId = Session.get('botId');
    let resource_id = Router.current().params.id;
    let temp = BotConversation.findOne({
      _id: botId
    })
    let data = temp.states.start.response
    let page = {
      type: "bot",
      id: botId
    }
    Meteor.call('addPagetoResource', resource_id, page, (err, result) => {
      if (result) {
        console.log('Success');
        Session.set('botID', "")
        Session.set('tags', "")
        Session.set('currentTag', "")
        Session.set('response', "")
        Session.set('botStart', '')
        Session.set('botResponse', false)
        Session.get('currentTemp', 'botStart')
        $('.nav-tabs a[href="#article"]').tab('show');
      }
    });
  }
});