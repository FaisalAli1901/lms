import { Template } from 'meteor/templating';

var tagContent = {}
var bot_tags
var botRespond

Template.userStartModel.onRendered(() => {
  botRespond = Session.get('botResponse')
  Session.set('tags', [])
  Session.set('userResponded', false)
});

Template.userStartModel.onDestroyed(() => {
  Session.set('currentTag', "")
})

Template.userStartModel.helpers({
  getTags() {
    let tags = Session.get('tags')
    let botSession = Session.get('botResponse')

    if (botSession) {
      let empty = []
      return empty
    } else {
      if (tags && tags.length > 0) {
        return tags
      }
    }
  },
  getTagName() {
    let tagName = Session.get('currentTag')
    if (tagName) {
      return tagName
    }
  }
});

Template.userStartModel.events({
  'click .addTags': (e, t) => {
    e.preventDefault();
    let tags = Session.get('tags')
    let tagName = t.find("#tagForm").value
    t.find("#tagForm").value = ''
    if (tags) {
      Session.set('botResponse', false)
      tags.push(tagName)
      Session.set('tags', tags)
    } else {
      Session.set('tags', [tagName])
    }
    setTimeout(() => {
      $('.tag-container').removeClass("active");
      $('.tag-container').last().addClass("active").trigger("click");
    }, 300)
  },
  'click .float-plus': (e) => {
    var jqEl = $(e.currentTarget);
    if (jqEl.hasClass('cross')) {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-enter-active slide-leave-active')
        .addClass('slide-leave-active')
        .css('display', 'none');
    } else {
      jqEl.siblings('.note-editor').find('.note-toolbar')
        .removeClass('slide-leave-active slide-enter-active')
        .addClass('slide-enter-active')
        .css('display', 'block');
    }
    jqEl.toggleClass('cross');
  },
  'click .tag-container': (e) => {
    e.preventDefault();
    $('.tag-container').removeClass("active");
    let jqEl = $(e.currentTarget),
        tag = jqEl.attr("id")
        summernoteInitialized = $('.summernote-right-tags').data("summernoteInitialized");

    let plusIcon = $('.float-plus')
    Session.set('currentTag', tag)
    jqEl.addClass("active");
    if(summernoteInitialized) {
      bot_tags.summernote('reset');
    } else {
      setTimeout(() => {
        bot_tags = $('.summernote-right-tags').summernote({
          placeholder: 'Add your Response',
          disableResizeEditor: true,
          height: "200",
          focus: true,
          toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['insert', ['link', 'picture', 'video']]
          ]
        });
        $('.summernote-right-tags').data("summernoteInitialized", true);
      }, 300)
    }
  },
  'click .save': (e, t) => {
    e.preventDefault();

    let tagName = e.target.id;
    let userResponse = bot_tags.summernote('code');

    tagContent[tagName] = [userResponse]
    bot_tags.summernote('reset');
  },
  'click .addTagsToServer': (e, t) => {
    e.preventDefault();
    let responses = Session.get('response')
    let id = Session.get('botId');
    let oldTag = Session.get('userTag');
    let tag = Session.get(tag)

    if (botRespond) {
      Meteor.call('updateUserStateBot', id, oldTag, tag, tagContent)
      Session.set('userResponded', true)
    } else {
      Meteor.call('updateUserState', id, tagContent);
    }

    responses.push('userResponse')
    Session.set('response', responses)
    Session.set('botStart', false)
    Session.set('currentTag', '')
    tagContent = {}
    Session.set('currentTemp', 'botResponse')
    Modal.hide('userStartModel')
  },
  'click .close': (e) => {
    e.preventDefault();
    Modal.hide('userStartModel')
  },
  'click .del': (e) => {
    e.preventDefault();
    let tags = Session.get('tags');
    let id = e.target.id
    let splitId = id.split('-')
    id = splitId[splitId.length - 1]
    let index = tags.indexOf(id)
    if (index > -1) {
      tags.splice(index, 1)
      Session.set('tags', tags)
    }
  }
});