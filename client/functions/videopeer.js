import UI from './ui';

export default class VideoPeer {
    constructor() {
        // this.peerServer = new Peer.peerServer
        this.peer = new Peer({ host: 'localhost', port: 9000, path: '/' });
        // this.peer = new Peer({ host: 'ec2-54-191-198-74.us-west-2.compute.amazonaws.com', port: 9000, path: '/' });
        this.ui = new UI();
        this.currentCall = null;
        this.local;
        navigator.mediaDevices.getUserMedia = navigator.mediaDevices.getUserMedia || navigator.mediaDevices.webkitGetUserMedia || navigator.mediaDevices.mozGetUserMedia;
        this.bindOnOpen();
        this.bindOnCall();
        this.bindOnError();
        this.bindOnClose();
    }

    getUserVideo() {
        navigator.getUserMedia({
            audio: true,
            video: true
        }, (stream) => {
            this.local = stream;

            $('#myvideo').prop("src", URL.createObjectURL(stream))
            window.localStream = stream;
        }, () => {
            alert("Error! Make sure to click allow when asked for permission by the browser");
        })
    }

    stopMediastream() {
        this.local.getTracks()[0].stop();
        this.local.getTracks()[1].stop();
        this.peer.destroy();
    }

    setPartnerVideo(call) {
        call.on('stream', (stream) => {
            $('#partnervideo').prop("src", URL.createObjectURL(stream))
        })
        this.ui.enterCall();
        this.currentCall = call;
    }

    callAKey(key) {
        console.log("call a key funtion");
        let call = this.peer.call(key, window.localStream);
        this.setPartnerVideo(call);
    }

    hangup() {
        console.log("hangup");
        if (this.currentCall) {
            this.currentCall.close();
            this.ui.leaveCall();
        }
    }

    bindOnOpen() {
        // var that = this;
        this.peer.on('open', (id) => {
            $(".key").html(id);
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $set: {
                    profile: {
                        peerId: id
                    }
                }
            })
            this.getUserVideo();
        });
    }

    bindOnCall() {
        this.peer.on('call', (call) => {
            call.answer(window.localStream);
            this.setPartnerVideo(call);
        });
    }

    bindOnError() {
        this.peer.on('error', (err) => {
            alert(err.message);
        });
    }

    bindOnClose() {
        this.peer.on('close', (err) => {
            this.ui.leaveCall();
        });
    }
}