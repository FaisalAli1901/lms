export default class TextPeer {
    constructor(){
        this.peer = new Peer({ key: '3oyncgfqskhxs9k9', debug: 3 });
        this.conn;
        this.receivedata();
    }
    createConnection(key){
        console.log('clicked');
        this.conn = this.peer.connect(key)
    }
    sendData(data){
        console.log('send data');
        console.log(this.conn);
        this.conn.on('open',  () => {
            console.log(data);
            this.conn.send(data);
        });
    }
    receivedata(){
        this.peer.on('connection', () => {
            this.conn.on('data', (data) => {
                console.log(data);
            })
        })
    }
}