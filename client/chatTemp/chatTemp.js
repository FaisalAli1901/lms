import { Template } from 'meteor/templating';

Template.chatTemp.helpers({ 
    getChat() { 
        Meteor.subscribe('currentChat');
        let temp = CurrentChat.find().fetch();
        return temp[0]
    }
}); 

Template.chatTemp.events({ 
    'click .videoCall': function(e) { 
        e.preventDefault()
        let name = e.target.id;

        Meteor.call('updateCurrentChat', name);
        Template.chatTemp.__helpers.get('getChat').call()
    } 
}); 
