import {
	Tracker
} from 'meteor/tracker'

let template;
let debateId;
let val

Template.debateDisagree.onCreated(() => {
	template = Template.instance();
	Meteor.subscribe('debate');

	template.userEdit = new ReactiveVar(false);
	template.userOpinion = new ReactiveVar();
	template.scrollVal = new ReactiveVar();
	debateId = Router.current().params.id

	Tracker.autorun(() => {
		template.userOpinion.set(Debate.findOne({ _id: debateId}))
	})
})

Template.debateDisagree.onRendered(() => {
	// console.log('onrendered');
	// setTimeout(() => {
	// 	template.scrollVal.set($('.showdown').offset().top)
	// 	val = template.scrollVal.get()
	// 	autosize($('textarea'))
	// }, 1000);

	$(window).scroll((e) => {
		let top = e.currentTarget.scrollY
		let commentsDiv = document.querySelector(".conversation-container");
		if (top > val) {
			$('.showdown').addClass('fixTop')
			$('.enterComment').removeClass('hidden')
			// $('.conversation-container').animate({
			// 	scrollTop: commentsDiv.scrollHeight
			// }, 1000);
			autosize($('textarea'))
		} else {
			$('.showdown').removeClass('fixTop')
			$('.enterComment').addClass('hidden')
			$(".conversation-container").stop()
		}
	})
})

Template.debateDisagree.helpers({
	getUserOpinion() {
		let temp = Template.instance().userOpinion.get()

		if (temp) {
			return temp
		}
	},
	userInput() {
		let userEdit = Template.instance().userEdit.get()
		let temp = Debate.findOne({})
		console.log(userEdit);
		if (temp.disagree && userEdit === false) {
			return true
		} else {
			return false
		}
	},
	userEdit() {
		let userEdit = Template.instance().userEdit.get()
		let data = Debate.findOne({})

		if (userEdit) {
			let tags = data.disagree.tags
			tags = tags.join(', ')
			return {
				statement: data.disagree.statement,
				tags: tags,
				detailStatement: data.disagree.detailStatement
			}
		} else {
			return true
		}
	},
	notStarted(data) {
		if (data === false) {
			return true
		} else {
			return false
		}
	},
	notEnded(data) {
		if (data === false) {
			return true
		} else {
			return false
		}
	},
	isEnded(data) {
		if (data === true) {
			return true
		} else {
			return false
		}
	},
	getUsername(userId) {
		if (userId === '2') {
			return 'Dr. Han Pen'
		} else {
			return 'Jenny Matthews'
		}
	},
	calPercent(agree, disagree) {
		let total = parseInt(agree) + parseInt(disagree)
		console.log(total);
		let agreePercent = Math.round((parseInt(agree) / total) * 100)
		let disagreePercent = Math.round((parseInt(disagree) / total) * 100)
		return {
			agreePercent,
			disagreePercent
		}
	},
	kFormator(data) {
		console.log(data);
		return data > 999 ? (data / 1000) + 'k' : data
	},
	getTemp(data) {
		if (data.userId === '2') {
			return {
				temp: 'thisDebator',
				data
			}
		} else {
			return {
				temp: 'otherDebator',
				data
			}
		}
	},
	setVal() {
		setTimeout(() => {
			template.scrollVal.set($('.showdown').offset().top);
			val = template.scrollVal.get()
			console.log('value:', val);
		}, 1000)
	}
});

Template.debateDisagree.events({
	'submit .userView': (e, t) => {
		e.preventDefault();
		let target = e.target
		let statement = target.statement.value.trim()
		let tags = target.tags.value.trim().split(',')
		let detailStatement = target.detailStatement.value.trim()
		let edit = Template.instance().userEdit.get()

		if (statement && tags && detailStatement) {
			if (edit) {
				Meteor.call('editUserInput', 'disagree', statement, tags, detailStatement);
				template.userEdit.set(false)
			} else {
				let data = {
					statement,
					tags,
					detailStatement
				}
				Meteor.call('createUserInput', 'disagree', data)
			}
		}
	},
	"click .edit": (e, t) => {
		e.preventDefault();
		template.userEdit.set(true)
	},
	"click .send": (e, t) => {
		e.preventDefault();
		let message = t.find('#userReply').value.trim()
		t.find('#userReply').value = ""
		autosize.destroy($('textarea'));
		if (message.length > 0) {
			Meteor.call('addConversation', message, '2', () => {
				// let commentsDiv = document.querySelector(".conversation-container");
				// $('.conversation-container').animate({
				// 	scrollTop: commentsDiv.scrollHeight
				// }, 1000);
			});
		}
	},
	'click .hideBtn': (e, t) => {
		e.preventDefault()
		Meteor.call('toggleStart', debateId);
	},
	'click .show': (e, t) => {
		e.preventDefault()
		Meteor.call('toggleStart', debateId);
	}
});
