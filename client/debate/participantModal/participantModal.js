let modalId
let debateId
let tId

Template.participantModal.onCreated(() => {
	debateId = Router.current().params.id
	console.log(debateId);
})

Template.participantModal.onRendered(() => {
	setTimeout(() => {
		var linkPreviews = $('.link-preview.unloaded');
		_.each(linkPreviews, (linkPreview) => {
			let qUrl = $(linkPreview).data("link");
			let objectUrl = LP.findOne({
				"url": qUrl
			});
			if (objectUrl) {
				$(linkPreview).find('.link-image').attr("src", objectUrl.image);
				$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(objectUrl.title, 100));
				$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(objectUrl.description, 200));
			} else {
				HTTP.call('GET', 'http://api.linkpreview.net/?key=59c881e95420219e6fdde211a88280424205b456ef721&q=' + qUrl, {}, function (error, response) {
					if (error) {
						console.log(error);
					} else {
						$(linkPreview).find('.link-image').attr("src", response.data.image);
						$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(response.data.title, 100));
						$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(response.data.description, 200));
						let lpData = {
							"url": qUrl,
							"image": response.data.image,
							"title": response.data.title,
							"description": response.data.description
						};
						Meteor.call('addLinkPreview', lpData, (result) => {
							console.log(result)
						});
					}
				});

			}
			$(linkPreview).removeClass('unloaded');
		});
	}, 1000);
})

Template.participantModal.helpers({
	getData(id) {
		console.log(id);
		modalId = id
		let temp = Debate.findOne({ _id: debateId });
		if (temp) {
			let conv = temp.conversations
			let filteredData
			conv.forEach((data) => {
				if (data.id === id) {
					filteredData = data
					tId = data.thread_id
				}
			});
			return filteredData
		}
	},
	getTemp(data) {
		if (data) {
			if (data.type === "seeks clarification") {
				return {
					temp: 'seekClarificationReply',
					data
				}
			} else {
				console.log(data);
				return {
					temp: 'counterFactsReply',
					data
				}
			}
		}
	}
});

Template.counterFactsReply.helpers({
	isLink(data) {
		return data ? true : false
	},
	userReply(replies) {
		if (replies && replies.length > 0) {
			console.log('called post replies');
			let userId = Meteor.userId()
			let ret
			replies.forEach((data) => {
				if (data.userId === userId) {
					console.log('true called');
					ret = true
				} else {
					console.log('false called');
					ret = false
				}
			})
			console.log(ret);
			return ret
		} else {
			return false
		}
	}
});

Template.seekClarificationReply.helpers({
	isLink(data) {
		return data ? true : false
	},
	userReply(replies) {
		if (replies && replies.length > 0) {
			console.log('called seek replies');
			let userId = Meteor.userId()
			let ret
			replies.forEach((data) => {
				if (data.userId === userId) {
					console.log('called true');
					ret =  true
				} else {
					ret = false
				}
			})
			return ret
		} else {
			return false
		}
	}
});

Template.participantModal.events({ 
	'click .timeline-left': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('increApplauseDebator', debateId, id);
		}
	}
});

Template.seekClarificationReply.events({
	'click .seekItem': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('increApplauseUser', debateId, modalId, id);
		}
	},
	'click .replyModal': (e, t) => {
		e.preventDefault()
		let commentId = e.target.id
		let cId = e.currentTarget.dataset.cid
		console.log("CID", cId);
		let data = {
			debateId,
			modalId,
			commentId,
			tId,
			cId
		}
		Modal.show('replyModal', data);
	},
	'click .seeEdit': (e, t) => {
		e.preventDefault()
		let commentId = e.target.id
		let data = {
			debateId,
			modalId,
			commentId,
			seeEdit: true
		}
		Modal.show('replyModal', data);
	}
});

Template.counterFactsReply.events({
	'click .clariItem': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		console.log(id);
		if (id) {
			Meteor.call('increApplauseUser', debateId, modalId, id);
		}
	},
	'click .replyModal': (e, t) => {
		e.preventDefault()
		let commentId = e.target.id
		let cId = e.currentTarget.dataset.cid
		console.log("CID", cId);
		let data = {
			debateId,
			modalId,
			commentId,
			tId,
			cId
		}
		Modal.show('replyModal', data);
	},
		'click .counterEdit': (e, t) => {
		e.preventDefault()
		let commentId = e.target.id
		let data = {
			debateId,
			modalId,
			commentId,
			seeEdit: true
		}
		Modal.show('replyModal', data);
	}
});