let data
let seeEdit
let template

Template.replyModal.onCreated(() => {
	template = Template.instance();

	template.link = new ReactiveVar();
	template.message = new ReactiveVar();
})

Template.replyModal.helpers({ 
	getIds(ids){
		if (ids.seeEdit) {
			data = {
				debateId: ids.debateId,
				modalId: ids.modalId,
				commentId: ids.commentId,
				tId: ids.tId,
				cId: ids.cId
			}
			seeEdit = ids.seeEdit
		} else {
			data = ids
		}
	},
	userReply(data){
		let debateId = data.debateId
		let convId = data.modalId
		let commentId = data.commentId

		if (data.seeEdit) {
			let temp = Debate.findOne({ _id: debateId });
			if (temp) {
				temp.conversations.forEach((data) => {
					if (data.id === convId) {
						data.comments.forEach((commet) => {
							if (commet.id === commentId) {
								let userId = Meteor.userId()
								commet.replies.forEach((reply) => {
									if (reply.userId === userId) {
										template.link.set(reply.link)
										template.message.set(reply.message)
									}
								})
							}
						})
					}
				});
			}
		}
	},
	getLink(){
		return Template.instance().link.get()
	},
	getMessage(){
		return Template.instance().message.get()
	}
}); 

Template.replyModal.events({ 
	'click .post': (e, t) => { 
		e.preventDefault()
		let link = t.find('#link').value.trim()
		let msg = t.find('#replyStatement').value.trim()
		if (seeEdit) {
			console.log('called if');
			Meteor.call('editReply', data.debateId, data.modalId, data.commentId, link, msg);
			Meteor.call('replyToComment', data.tId, data.cId, msg);
		} else {
			console.log('called else');
			Meteor.call('postReply', data.debateId, data.modalId, data.commentId, link, msg);
			Meteor.call('replyToComment', data.tId, data.cId, msg);
		}
		Modal.hide('replyModal');
	} 
}); 
