Template.clarificationModal.events({
	'click .post': (e, t) => {
		e.preventDefault()
		let link = $('#link').val().trim()
		let facts = $('#clariStatement').val().trim()
		let debateId = Router.current().params.id
		let id = e.target.id

		console.log(id);

		if (link && facts) {
			Modal.hide('clarificationModal')
			Meteor.call('addFacts', debateId, id, link, facts, 'seeks clarification');
			Meteor.call('addThreadAndComments', debateId, id, facts)
			setTimeout(() => {
				var linkPreviews = $('.link-preview.unloaded');
				_.each(linkPreviews, (linkPreview) => {
					let qUrl = $(linkPreview).data("link");
					let objectUrl = LP.findOne({
						"url": qUrl
					});
					if (objectUrl) {
						$(linkPreview).find('.link-image').attr("src", objectUrl.image);
						$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(objectUrl.title, 100));
						$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(objectUrl.description, 200));
					} else {
						HTTP.call('GET', 'http://api.linkpreview.net/?key=59c881e95420219e6fdde211a88280424205b456ef721&q=' + qUrl, {}, function (error, response) {
							if (error) {
								console.log(error);
							} else {
								$(linkPreview).find('.link-image').attr("src", response.data.image);
								$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(response.data.title, 100));
								$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(response.data.description, 200));
								let lpData = {
									"url": qUrl,
									"image": response.data.image,
									"title": response.data.title,
									"description": response.data.description
								};
								Meteor.call('addLinkPreview', lpData, (result) => {
									console.log(result)
								});
							}
						});

					}
					$(linkPreview).removeClass('unloaded');
				});
			}, 1000);
		} else if (facts) {
			Modal.hide('clarificationModal')
			Meteor.call('addFacts', debateId, id, '', facts, 'seeks clarification');
			Meteor.call('addThreadAndComments', debateId, id, facts)
		}
	}
});