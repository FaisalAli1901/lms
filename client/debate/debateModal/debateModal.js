let modalId
let debateId

Template.debateModal.onCreated(() => {
	debateId = Router.current().params.id
	console.log(debateId);
})

Template.debateModal.onRendered(() => {
	setTimeout(() => {
		var linkPreviews = $('.link-preview.unloaded');
		_.each(linkPreviews, (linkPreview) => {
			let qUrl = $(linkPreview).data("link");
			let objectUrl = LP.findOne({
				"url": qUrl
			});
			if (objectUrl) {
				$(linkPreview).find('.link-image').attr("src", objectUrl.image);
				$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(objectUrl.title, 100));
				$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(objectUrl.description, 200));
			} else {
				HTTP.call('GET', 'http://api.linkpreview.net/?key=59c881e95420219e6fdde211a88280424205b456ef721&q=' + qUrl, {}, function (error, response) {
					if (error) {
						console.log(error);
					} else {
						$(linkPreview).find('.link-image').attr("src", response.data.image);
						$(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(response.data.title, 100));
						$(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(response.data.description, 200));
						let lpData = {
							"url": qUrl,
							"image": response.data.image,
							"title": response.data.title,
							"description": response.data.description
						};
						Meteor.call('addLinkPreview', lpData, (result) => {
							console.log(result)
						});
					}
				});

			}
			$(linkPreview).removeClass('unloaded');
		});
	}, 1000);
})

Template.debateModal.helpers({
	getData(id) {
		modalId = id
		let temp = Debate.findOne({_id: debateId});
		console.log(temp);
		if (temp) {
			let conv = temp.conversations
			console.log(conv);
			let filteredData
			conv.forEach((data) => {
				if (data.id === id) {
					filteredData = data
				}
			});
			return filteredData
		}
	},
	getTemp(data){
		console.log(data);
		if(data){
			if (data.type === "seeks clarification") {
				return {
					temp: 'seekClarification',
					data
				}
			} else {
				return {
					temp: 'counterFacts',
					data
				}
			}
		}
	},
});

Template.counterFacts.helpers({
	isLink(data) {
		return data ? true : false
	}
});

Template.seekClarification.helpers({
	isLink(data) {
		return data ? true : false
	}
});


Template.debateModal.events({
	'click #counter-fact': (e, t) => {
		e.preventDefault();
		id = e.target.className
		Modal.show('factsModal', id);
	},
	'click #clarification': (e, t) => {
		e.preventDefault();
		id = e.target.className
		Modal.show('clarificationModal', id);
	},
	'click .timeline-left': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('increApplauseDebator', debateId, id);
		}
	}
});

Template.seekClarification.events({
	'click .seekItem': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('increApplauseUser', debateId, modalId, id);
		}
	},
	'click .up': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id

		if (id) {
			Meteor.call('like', debateId, modalId, id);
		}
	},
	'click .down': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('dislike', debateId, modalId, id);
		}
	}
});

Template.counterFacts.events({
	'click .clariItem': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		console.log(id);
		if (id) {
			Meteor.call('increApplauseUser', debateId, modalId, id);
		}
	},
	'click .up': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('like', debateId, modalId, id);
		}
	},
	'click .down': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('dislike', debateId, modalId, id);
		}
	}
});