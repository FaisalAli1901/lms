let template;
let val
let debateId

Template.debate.onCreated(() => {
	template = Template.instance();
	Meteor.subscribe('debate');

	template.userOpinion = new ReactiveVar();
	template.userInput = new ReactiveVar(false);
	template.userViewVal = new ReactiveVar();
	template.userEdit = new ReactiveVar(false);
	template.scrollVal = new ReactiveVar();

	debateId = Router.current().params.id
})

Template.debate.onRendered(() => {
	// setTimeout(() => {
	// 	if ($('.showdownNav')) {
	// 		template.scrollVal.set($('.showdownNav').offset().top)
	// 		val = template.scrollVal.get()
	// 	}
	// }, 1000);



	$(window).scroll((e) => {
		let top = e.currentTarget.scrollY
		if (top > val) {
			$('.showdownNav').addClass('fixTop')
			$('.showdown-timeline').addClass('topPadding')
		} else {
			$('.showdownNav').removeClass('fixTop')
			$('.showdown-timeline').removeClass('topPadding')
		}
	})


})

Template.debate.helpers({
	getUserOpinion() {
		let temp = Debate.findOne({_id: debateId})
		if (temp) {

			let userTakes = temp.participantTake.filter((data) => {
				if (data.userId === Meteor.userId()) {
					return data
				}
			})

			if (userTakes.length > 0) {
				template.userOpinion.set(userTakes[0].takePoint);
				template.userInput.set(true)
				template.userViewVal.set(userTakes[0].takeMsg)
			}else{
				template.userOpinion.set('50')
			}

			return temp
		}
	},
	getUsername(userId) {
		if (userId === '2') {
			return 'Dr. Han Pen'
		} else {
			return 'Jenny Matthews'
		}
	},
	calPercent(agree, disagree) {
		let total = parseInt(agree) + parseInt(disagree)
		let agreePercent = Math.round((parseInt(agree) / total) * 100)
		let disagreePercent = Math.round((parseInt(disagree) / total) * 100)
		return {
			agreePercent,
			disagreePercent
		}
	},
	kFormator(data) {
		return data > 999 ? (data / 1000) + 'k' : data
	},
	getTemp(data) {
		if (data.userId === '1') {
			return {
				temp: 'debateAgreeTemp',
				data
			}
		} else {
			return {
				temp: 'debateDisagreeTemp',
				data
			}
		}
	},
	userOpinion() {
		let opinion = Template.instance().userOpinion.get();
		switch (opinion) {
			case '0':
				return 'I totally disagree on this topic'
				break;
			case '25':
				return 'I almost disagree on this topic'
				break;
			case '50':
				return 'I am neutral on this topic'
				break;
			case '75':
				return 'I almost agree on this topic'
				break;
			case '100':
				return 'I totally agree on this topic'
				break;
		}
	},
	opinion() {
		let opinion = Template.instance().userOpinion.get();

		if (opinion) {
			return true;
		}
	},
	opinionVal() {
		return Template.instance().userOpinion.get();
	},
	question() {
		let opinion = Template.instance().userOpinion.get();
		switch (opinion) {
			case '0':
				return 'Why are you totally disagreeing with this topic ?'
				break;
			case '25':
				return 'Why are you almost disagreeing with this topic ?'
				break;
			case '50':
				return 'Why are you neutral with this topic ?'
				break;
			case '75':
				return 'Why are you almost agreeing with this topic ?'
				break;
			case '100':
				return 'Why are you totally agreeing with this topic ?'
				break;

		}
	},
	userInput() {
		return Template.instance().userInput.get();
	},
	userViewVal() {
		return Template.instance().userViewVal.get();
	},
	editVal() {
		let val = Template.instance().userViewVal.get();
		let edit = Template.instance().userEdit.get();
		if (edit) {
			return val
		}
	},
	setVal() {
		setTimeout(() => {
			template.scrollVal.set($('.showdownNav').offset().top);
			val = template.scrollVal.get()
		}, 1000)
	}
});



Template.debate.events({
	'change #userOption': (e, t) => {
		let val = e.target.value
		template.userOpinion.set(val);
	},
	'click .postBtn': (e, t) => {
		e.preventDefault()
		let val = t.find('#userInput').value.trim()
		let takePoint = Template.instance().userOpinion.get()
		let edit = Template.instance().userEdit.get()
		let userId = Meteor.userId()

		// template.userViewVal.set(val)

		if (val.length > 0) {
			if (edit) {
				Meteor.call('editParticipantTake', debateId, userId, takePoint, val);
				template.userInput.set(true)
			} else {
				Meteor.call('addParticipantTake', debateId, userId, takePoint, val);
				template.userInput.set(true)
			}
		}

	},
	'click .btnEdit': (e, t) => {
		e.preventDefault()
		template.userInput.set(false)
		template.userEdit.set(true)
	},
	'click .disagreeSpeaker': (e, t) => {
		e.preventDefault()
		$('.start-header').fadeOut(100, () => {
			$('.start-header').addClass('exit')
			$('.debate-main').addClass('hide')
			$('.results-section').addClass('hide')
			$('#show-down').addClass('hide')
			$('.conclusion-timeline-section').addClass('hide')
		});

		$('.leftSpeaker').fadeIn(102, () => {
			$('.meetGuest').addClass('hide')
			$('#intro-timeline').addClass('zero-top')
			$('.participants-details-page').addClass('zero-top')
			$('.leftSpeaker').addClass('enter')
		})
	},
	'click .agreeSpeaker': (e, t) => {
		e.preventDefault()
		$('.start-header').fadeOut(100, () => {
			$('.start-header').addClass('exit')
			$('.debate-main').addClass('hide')
			$('.results-section').addClass('hide')
			$('#show-down').addClass('hide')
			$('.conclusion-timeline-section').addClass('hide')
		});

		$('.rightSpeaker').fadeIn(102, () => {
			$('.meetGuest').addClass('hide')
			$('#intro-timeline').addClass('zero-top')
			$('.participants-details-page').addClass('zero-top')
			$('.rightSpeaker').addClass('enter-right')
		})
	},
	'click .left-main': (e, t) => {
		e.preventDefault()

		$('.participants-details-page').fadeOut(100, () => {
			$('.meetGuest').removeClass('hide')
			$('.leftSpeaker').removeClass('enter-right')
		})

		$('.start-header').fadeIn(102, () => {
			$('.results-section').removeClass('hide')
			$('.debate-main').removeClass('hide')
			$('#show-down').removeClass('hide')
			$('.conclusion-timeline-section').removeClass('hide')
			$('#intro-timeline').removeClass('zero-top')
			$('.participants-details-page').removeClass('zero-top')
			$('.start-header').removeClass('exit')
			let val = $('.meetGuest').offset().top
			$('html, body').animate({
				scrollTop: val - 64
			}, 1000);
		});

	},
	'click .right-main': (e, t) => {
		e.preventDefault()

		$('.participants-details-page').fadeOut(100, () => {
			$('.meetGuest').removeClass('hide')
			$('.rightSpeaker').removeClass('enter')
		})

		$('.start-header').fadeIn(102, () => {
			$('.results-section').removeClass('hide')
			$('.debate-main').removeClass('hide')
			$('#show-down').removeClass('hide')
			$('.conclusion-timeline-section').removeClass('hide')
			$('#intro-timeline').removeClass('zero-top')
			$('.participants-details-page').removeClass('zero-top')
			$('.start-header').removeClass('exit')
			let val = $('.meetGuest').offset().top
			$('html, body').animate({
				scrollTop: val - 64
			}, 1000);
		});

	},
	'click .scroll-icon': (e, t) => {
		e.preventDefault()
		let val = $('.meetGuest').offset().top
		$('html, body').animate({
			scrollTop: val - 64
		}, 1000);
	},
	'click .downArrow': (e, t) => {
		e.preventDefault()
		let val = $('.results-section').offset().top
		$('html, body').animate({
			scrollTop: val
		}, 1000);
	},
	'click .scroll-btn': (e, t) => {
		e.preventDefault()
		let val = $('#show-down').offset().top
		$('html, body').animate({
			scrollTop: val + 2
		}, 1000);
	},
	'click .scrollTop': (e, t) => {
		e.preventDefault()
		let val = $('.debate-main').offset().top
		$('html, body').animate({
			scrollTop: val - 64
		}, 1000);
	},
	'click .timeline-left': (e, t) => {
		e.preventDefault()
		let id = e.currentTarget.dataset.id
		if (id) {
			Meteor.call('increApplauseDebator', debateId, id);
		}
	}
});

Template.debateAgreeTemp.events({
	'click .openModal': (e, t) => {
		e.preventDefault()
		let Id = e.target.id
		Modal.show('debateModal', Id);
	}
});

Template.debateDisagreeTemp.events({
	'click .openModal': (e, t) => {
		e.preventDefault()
		let Id = e.target.id
		setTimeout(() => {
			Modal.show('debateModal', Id);
		}, 300)
	}
});