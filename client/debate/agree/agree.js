import {
	Tracker
} from 'meteor/tracker'


let template;
let debateId;
let val

Template.debateAgree.onCreated(() => {
	template = Template.instance();
	Meteor.subscribe('debate');

	template.userEdit = new ReactiveVar(false);
	template.userOpinion = new ReactiveVar();
	template.scrollVal = new ReactiveVar();
	debateId = Router.current().params.id
	console.log(debateId);

	Tracker.autorun(() => {
		template.userOpinion.set(Debate.findOne({_id: debateId}))
	})
})

Template.debateAgree.onRendered(() => {
	// setTimeout(() => {
	// 	template.scrollVal.set($('.showdown').offset().top)
	// 	val = template.scrollVal.get()
	// 	autosize($('textarea'))
	// }, 1000);

	$(window).scroll((e) => {
		let top = e.currentTarget.scrollY
		let commentsDiv = document.querySelector(".conversation-container");
		if (top > val) {
			$('.showdown').addClass('fixTop')
			$('.enterComment').removeClass('hidden')
			// // commentsDiv.scrollTop = commentsDiv.scrollHeight;
			// $('.conversation-container').animate({
			// 	scrollTop: commentsDiv.scrollHeight
			// }, 1000);
			autosize($('textarea'))
		} else {
			$('.showdown').removeClass('fixTop')
			$('.enterComment').addClass('hidden')
			
			// debugger
			// commentsDiv.scrollTop = document.querySelector("html,body").scrollHeight - e.originalEvent.wheelDelta;
		}
	})
	
})

Template.debateAgree.helpers({
	getUserOpinion() {
		let temp = Template.instance().userOpinion.get()
		if (temp) {
			return temp
		}
	},
	userInput() {
		let userEdit = Template.instance().userEdit.get()
		let temp = Debate.findOne({})
		console.log(userEdit);
		if (temp.agree && userEdit === false) {
			return true
		} else {
			return false
		}
	},
	userEdit() {
		let userEdit = Template.instance().userEdit.get()
		let data = Debate.findOne({})

		if (userEdit) {
			let tags = data.agree.tags
			tags = tags.join(', ')
			return {
				statement: data.agree.statement,
				tags: tags,
				detailStatement: data.agree.detailStatement
			}
		} else {
			return true
		}
	},
	notStarted(data) {
		if (data === false) {
			return true
		} else {
			return false
		}
	},
	notEnded(data) {
		if (data === false) {
			return true
		} else {
			return false
		}
	},
	isEnded(data) {
		if (data === true) {
			return true
		} else {
			return false
		}
	},
	getUsername(userId) {
		if (userId === '2') {
			return 'Dr. Han Pen'
		} else {
			return 'Jenny Matthews'
		}
	},
	calPercent(agree, disagree) {
		let total = parseInt(agree) + parseInt(disagree)
		console.log(total);
		let agreePercent = Math.round((parseInt(agree) / total) * 100)
		let disagreePercent = Math.round((parseInt(disagree) / total) * 100)
		return {
			agreePercent,
			disagreePercent
		}
	},
	kFormator(data) {
		console.log(data);
		return data > 999 ? (data / 1000) + 'k' : data
	},
	getTemp(data) {
		if (data.userId === '1') {
			return {
				temp: 'thisDebator',
				data
			}
		} else {
			return {
				temp: 'otherDebator',
				data
			}
		}
	},
		setVal() {
			setTimeout(() => {
				template.scrollVal.set($('.showdown').offset().top);
				val = template.scrollVal.get()
				console.log('value:', val);
			}, 1000)
		}
});

Template.registerHelper('getUserpic', (id) => {
	if (id === '1') {
		return '/debate/Jenny.png'
	} else {
		return '/debate/han.png'
	}
});

Template.registerHelper('getUsername', (id) => {
	if (id === 1) {
		return 'Jenny Matthews'
	} else {
		return 'Dr. Han Pen'
	}
});

Template.registerHelper('attachments', (data) => {
	console.log(data ? true : false);
	if (data) {
		return true
	}
})

Template.registerHelper('getCount', (length, sub) => {
	console.log(length);
	if (length > 6) {
		let count = parseInt(length) - parseInt(sub);
		return count;
	} else {
		return 6
	}

})

Template.debateAgree.events({
	'submit .userView': (e, t) => {
		e.preventDefault();
		let target = e.target
		let statement = target.statement.value.trim()
		let tags = target.tags.value.trim().split(',')
		let detailStatement = target.detailStatement.value.trim()
		let edit = Template.instance().userEdit.get()

		if (statement && tags && detailStatement) {
			if (edit) {
				Meteor.call('editUserInput', 'agree', statement, tags, detailStatement);
				template.userEdit.set(false)
			} else {
				let data = {
					statement,
					tags,
					detailStatement
				}
				Meteor.call('createUserInput', 'agree', data)
			}
		}
	},
	"click .edit": (e, t) => {
		e.preventDefault();
		template.userEdit.set(true)
	},
	"click .send": (e, t) => {
		e.preventDefault();
		let message = t.find('#userReply').value.trim()
		t.find('#userReply').value = ""
		autosize.destroy($('textarea'));
		if (message.length > 0) {
			Meteor.call('addConversation', message, '1', () => {
					// let commentsDiv = document.querySelector(".conversation-container");
					// $('.conversation-container').animate({
					// 	scrollTop: commentsDiv.scrollHeight
					// }, 1000);
			});
		}
	},
	'click .hideBtn': (e, t) => {
		e.preventDefault()
		Meteor.call('toggleStart', debateId);
	},
	'click .show': (e, t) => {
		e.preventDefault()
		Meteor.call('toggleStart', debateId);
	}
});


Template.otherDebator.events({
	'click .openModal': (e, t) => {
		e.preventDefault()
		let Id = e.target.id
		Modal.show('participantModal', Id);
	}
});

Template.thisDebator.events({
	'click .openModal': (e, t) => {
		e.preventDefault()
		let Id = e.target.id
		Modal.show('participantModal', Id);
	}
});