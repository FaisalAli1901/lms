import { Template } from 'meteor/templating';

Template.dashboard.helpers({
    getUserInfo: () => {
        Meteor.subscribe("users");

        let user = Meteor.users.find({ _id: Meteor.userId() }).fetch()
        
        if (user[0]) {
            return user[0].emails[0]
        }
        
    }
});