import { Template } from 'meteor/templating';

Template.quizTemp.helpers({
    getArticle() {
        let id = Router.current().params.id

        let temp = Quizzes.findOne({_id: id});
        console.log(temp);
        if (temp) {
            return temp;
        }
    }
});