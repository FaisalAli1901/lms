import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

Template.viewer.onRendered(function user2OnCreated() {

    var mousePressed = false;
    var lastX2, lastY2;
    var ctx = document.getElementById('viewerCanvas').getContext("2d");

    let creatorId = Router.current().params.id
    let currentUser = Meteor.userId();

    Canvas.find({ author: creatorId, consumer: currentUser }).observeChanges({

        added(id, fields) {
            var x2 = fields.x
            var y2 = fields.y
            var isDown = fields.isDown
            if(isDown){
                ctx.beginPath();
                ctx.strokeStyle = $('#selColor').val();
                ctx.lineWidth = $('#selWidth').val();
                ctx.lineJoin = "round";
                ctx.moveTo(lastX2, lastY2);
                ctx.lineTo(x2, y2);
                ctx.closePath();
                ctx.stroke();
            }
            lastX2 = x2;
            lastY2 = y2;
        }
    });

})
