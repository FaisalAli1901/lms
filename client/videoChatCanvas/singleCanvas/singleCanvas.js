import { Template } from 'meteor/templating';

Template.singleView.onRendered(function helloOnCreated() {

    var mousePressed = false;
    var lastX2, lastY2;
    var canvas = document.getElementById('singleView')
    var ctx = canvas.getContext("2d");

    SingleCanvas.find().observeChanges({

        added: function (id, fields) {
            var x2 = fields.x
            var y2 = fields.y

            ctx.beginPath();
            ctx.strokeStyle = $('#selColor').val();
            ctx.lineWidth = $('#selWidth').val();
            ctx.lineJoin = "round";
            ctx.moveTo(lastX2, lastY2);
            ctx.lineTo(x2, y2);
            ctx.closePath();
            ctx.stroke();
            lastX2 = x2;
            lastY2 = y2;
        }
    });




    var mousePressed = false;
    var lastX, lastY;
    // var ctx, ctx2;
    // ctx = document.getElementById('myCanvas').getContext("2d");

    $('#singleView').mousedown(function (e) {
        console.log($(this).offset().left)

        mousePressed = true;
        Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
    });

    $('#singleView').mousemove(function (e) {
        if (mousePressed) {
            Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
        }
    });

    $('#singleView').mouseup(function (e) {
        mousePressed = false;
    });
    $('#singleView').mouseleave(function (e) {
        mousePressed = false;
    });
    // }

    function Draw(x, y, isDown) {
        if (isDown) {
            ctx.beginPath();
            ctx.strokeStyle = $('#selColor').val();
            ctx.lineWidth = $('#selWidth').val();
            ctx.lineJoin = "round";
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(x, y);
            ctx.closePath();
            ctx.stroke();
            SingleCanvas.insert({ "x": x, "y": y })
        }
        lastX = x;
        lastY = y;
    }

    function clearArea() {
        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }
});