import { Template } from 'meteor/templating';

Template.creator.onRendered(function helloOnCreated() {
    var mousePressed = false;
    var ctx = document.getElementById('creatorCanvas').getContext("2d");

    let consumerId = Router.current().params.id
    let currentUser = Meteor.userId();
    
    var lastX, lastY;
    // var ctx, ctx2;
    // ctx = document.getElementById('myCanvas').getContext("2d");

    $('#creatorCanvas').mousedown(function (e) {
        // console.log($(this).offset().left)

        mousePressed = true;
        Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
    });

    $('#creatorCanvas').mousemove(function (e) {
        if (mousePressed) {
            Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
        }
    });

    $('#creatorCanvas').mouseup(function (e) {
        mousePressed = false;
        Canvas.insert({ "author": currentUser, "consumer": consumerId, "isDown": false })
    });
    $('#creatorCanvas').mouseleave(function (e) {
        mousePressed = false;
    });
    // }

    function Draw(x, y, isDown) {
        if (isDown) {
            ctx.beginPath();
            ctx.strokeStyle = $('#selColor').val();
            ctx.lineWidth = $('#selWidth').val();
            ctx.lineJoin = "round";
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(x, y);
            ctx.closePath();
            ctx.stroke();
            Canvas.insert({ "author": currentUser, "consumer": consumerId, "x": x, "y": y, "isDown": true })
            // Meteor.call('addToCanvas', currentUser, x, y);
        }
        lastX = x;
        lastY = y;
    }

    function clearArea() {
        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }
});