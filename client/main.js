import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

Template.layout.onRendered(() => {
  let mouseY = 0;

  document.addEventListener('mousemove', function (e) {
    mouseY = e.clientY || e.pageY;
    let windowHeight = window.innerHeight,
      footerHeight = $('.pages-footer').height()
      ;
    if ((windowHeight - mouseY) < footerHeight) {
      $('.pages-footer').css({
        bottom: '0'
      });
    } else {
      $('.pages-footer').css({
        bottom: -footerHeight
      });
      $('.toc-jumbotron').addClass('hidden');
    }
  }, false);

});

Meteor.startup(() => {
	Modal.allowMultiple = true
});