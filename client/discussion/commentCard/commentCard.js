import { Template } from 'meteor/templating';

Template.commentCard.helpers({
	getComment() {
		let comment_id = Template.instance().data['comment_id'];
		if(comment_id) {
			return Comment.findOne({ _id: comment_id });
		}
	}
});
