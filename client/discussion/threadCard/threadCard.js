import { Template } from 'meteor/templating';

Template.threadCard.onCreated(() => {
	Meteor.subscribe('comments');
});

Template.threadCard.onRendered(() => {
	var slickCommentsList = $(".list-comment").filter(function() {
		if ($.trim($(this).html()) === "") {
			$(this).html('');
		}
		return $.trim($(this).html()) !== "";
	});

	$.each( slickCommentsList, function( key, value ) {
		$(value).owlCarousel({
			items: 2,
			autoWidth: true,
			loop: true,
			lazyLoad: true,
			dots: false
		});
	});

})

Template.threadCard.helpers({
	getThread() {
		Meteor.subscribe('comments');
		let thread_id = Template.instance().data['thread_id'];
		if(thread_id) {
			return Thread.findOne({ _id: thread_id });
		}
	},
  getComments() {
		Meteor.subscribe('comments');
    let thread_id = Template.instance().data['thread_id'];
    if (thread_id) {
      return Comment.find({ thread_id: thread_id });
    }
  }
});

Template.threadCard.events({
  'click .up-trend': function(e, template) {
		e.preventDefault();
		let thread_id = $(e.currentTarget).data("threadId"),
				userId = Meteor.user()._id;

    Meteor.call('updateUpVote', thread_id, userId);
  },
  'click .down-trend': function(e, template) {
		e.preventDefault();
		let thread_id = $(e.currentTarget).data("threadId"),
				userId = Meteor.user()._id;

    Meteor.call('updateDownVote', thread_id, userId);
  }
});

