Template.registerHelper("readableDate", function (timestamp) {
  return new Date(timestamp).toString('yyyy-MM-dd')
});

var activeUser = 0;

var DateFormats = {
  time: "HH:mmA",
  short: "MMMM DD [at] HH:mma",
  date: "MMMM dddd[,] YYYY",
  long: "dddd DD.MM.YYYY HH:mm"
};

UI.registerHelper("formatDate", function (datetime, format) {
  if (moment) {
    // can use other formats like 'lll' too
    format = DateFormats[format] || format;
    return moment(datetime).format(format);
  } else {
    return datetime;
  }
});

Template.registerHelper("readabaleDate", function (timestamp) {
  return new Date(timestamp).toString('yyyy-MM-dd')
});

Template.discussion.onCreated(() => {
  Meteor.subscribe('linkPreviews');
  Meteor.subscribe('discussions');
  Meteor.subscribe('discussion', Router.current().params.did);
  Meteor.subscribe('threads');
  Meteor.subscribe('comments');

  let discussion_id = Router.current().params.did;
  Meteor.call('filterMostUpvotedThread', discussion_id, (error, result) => {
    Session.set('mostUpvotedThread', result);
  });
  Meteor.call('filterMostCommentedThread', discussion_id, (error, result) => {
    Session.set('mostCommentedThread', result);
  });
});

Template.discussion.onRendered(() => {
  setTimeout(() => {
    let qUrl = $('.link-preview').data("link");
    let objectUrl = LP.findOne({
      "url": qUrl
    });
    if (objectUrl) {
      $('.link-image').attr("src", objectUrl.image);
      $('.link-title').text(UI._globalHelpers.limitPara(objectUrl.title, 100));
      $('.link-description').text(UI._globalHelpers.limitPara(objectUrl.description, 200));
    } else {
      HTTP.call('GET', 'http://api.linkpreview.net/?key=59c881e95420219e6fdde211a88280424205b456ef721&q=' + qUrl, {}, function (error, response) {
        if (error) {
          console.log(error);
        } else {
          $('.link-image').attr("src", response.data.image);
          $('.link-title').text(UI._globalHelpers.limitPara(response.data.title, 100));
          $('.link-description').text(UI._globalHelpers.limitPara(response.data.description, 200));

          LP.insert({
            "url": qUrl,
            "image": response.data.image,
            "title": response.data.title,
            "description": response.data.description
          })
        }
      });
    }

    $('textarea.join-the-thread').summernote({
      placeholder: 'start typing',
      disableResizeEditor: false,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['table', 'link', 'picture', 'video']]
      ],
      focus: true,
      callbacks: {
        onImageUpload: function (files, editor, welEditable) {
          for (var i = files.length - 1; i >= 0; i--) {
            sendFile(files[i], this);
          }

          function sendFile(file, el) {
            data = new FormData();
            data.append("file", file);
            FS.Utility.eachFile(event, function (file) {
              Images.insert(file, function (err, fileObj) {
                if (err) {
                  console.log("error in inserting the image");
                } else {
                  var imagesURL = "/cfs/files/images/" + fileObj._id;
                  setTimeout(() => {
                    $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                    $('[name="coverPic"]').val(location.origin + imagesURL);
                  }, 1000);
                }
              });
            });
          }
        }
      }
    });

  }, 2000)


});

Template.discussion.helpers({
  getDiscussion() {
    let discussion_id = Router.current().params.did;
    if (discussion_id) {
      return Discussion.findOne({
        _id: discussion_id
      });
    }
  },
  getThreads() {
    let discussion_id = Router.current().params.did;
    if (discussion_id) {
      return Thread.find({
        discussion_id: discussion_id
      });
    }
  },
  less_than_3(index) {
    if (index >= 3) {
      activeUser++
      return false
    } else {
      return true
    }
  },
  getCount() {
    return activeUser - 1;
  },
});

/////// edit/new discussion
var readDiscussionForm = function (template) {
  var discussion = {}
  discussion.author = Meteor.user()._id;
  discussion.title = template.find('[name=title]').value;
  discussion.body = template.find('[name=body]').value;
  discussion.link = template.find('[name=link]').value;
  discussion.createdAt = Date.now();
  discussion.threads = [];
  discussion.agree = [];
  discussion.disagree = [];
  discussion.like = [];
  discussion.bookmark = [];
  discussion.users = [];

  return discussion;
}

Template.discussionForm.events({
  'submit form': function (e, template) {
    e.preventDefault();

    var changes = readDiscussionForm(template);
    Meteor.call('createDiscussion', changes, (error, result) => {
      Modal.hide('discussionForm');
      Router.go(`/discussions/${result}`);
    });
  }
});

Template.discussion.events({
  'click .follow-user': function (e, template) {
    e.preventDefault();
    let el = $(e.currentTarget)
    $(el).toggleClass('active');
    $(el).find('.followed').toggleClass('hidden');
  },
  'click .likes': function (e, template) {
    e.preventDefault();
    let discussionId = $(e.currentTarget).data("discussionId"),
      userId = Meteor.user()._id;

    Meteor.call('updateAgree', discussionId, userId);
  },
  'click .dislikes': function (e, template) {
    e.preventDefault();
    let discussionId = $(e.currentTarget).data("discussionId"),
      userId = Meteor.user()._id;

    Meteor.call('updateDisagree', discussionId, userId);
  },
  'click .love': function (e, template) {
    e.preventDefault();
    let discussionId = $(e.currentTarget).data("discussionId"),
      userId = Meteor.user()._id;

    Meteor.call('updateLike', discussionId, userId);
  },
  'click .bookmark': function (e, template) {
    e.preventDefault();
    let discussionId = $(e.currentTarget).data("discussionId"),
      userId = Meteor.user()._id;

    Meteor.call('updateBookmark', discussionId, userId);
  }
});

Template.most_upvoted_thread.helpers({
  getMostUpvotedThread() {
    return Session.get('mostUpvotedThread');
  }
});

Template.most_commented_thread.helpers({
  getMostCommentedThread() {
    return Session.get('mostCommentedThread');
  }
});
