Template.body.helpers({
  discussionsListCursor: function() {
    return Discussion.list;
  }
});

Template.discussionList.onCreated(() => {
  Meteor.subscribe('linkPreviews');
  Meteor.subscribe('discussions');
  Meteor.subscribe('users');
});

Template.componentList.created = function() {
  var self = this;
  self.limitBy = this.data.limitBy || 2;
  self.limit = new ReactiveVar(self.limitBy);
  self.autorun(function() {
    self.subscription = Meteor.subscribe(self.data.subscription, self.limit.get());
  });
};


Template.discussionList.onRendered(() => {
  localStorage.setItem("horizontalScrollValue", "20");
  setTimeout(()  => {
    var slickThreadsList  = $(".list-comment.unloaded").filter(function() {
      if ($.trim($(this).html()) === "") {
        $(this).html('');
      }
      return $.trim($(this).html()) !== "";
    });

    $.each( slickThreadsList , function( key, value ) {
      $(value).owlCarousel({
        items: 2,
        autoWidth: true,
        loop: true,
        lazyLoad: true,
        dots: false
      });
      $(value).removeClass('unloaded');
    });

  }, 3000);
});

Template.componentList.helpers({
  items: function() {
    // return this.cursor({ limit: Template.instance().limit.get() });
  },
  showLoadButton: function() {
    return Counts.get(Template.instance().data.subscription) > Template.instance().limit.get();
  },
  loading: function() {
    return Template.instance().subscription &&
      ! Template.instance().subscription.ready();
  }
});

Template.componentList.events({
  'click [data-load]': function(event, template) {
    event.preventDefault()
    // debugger
    template.limit.set(template.limit.get() + template.limitBy);
    let scrollValue = parseInt(localStorage.getItem("horizontalScrollValue"));
    localStorage.setItem("horizontalScrollValue", scrollValue + (478 * template.limitBy));
    $('html, body').animate({
      scrollLeft: localStorage.getItem("horizontalScrollValue")
    }, 300);
    setTimeout( () => {

      var linkPreviews = $('.link-preview.unloaded');
      _.each(linkPreviews, (linkPreview) => {
        let qUrl = $(linkPreview).data("link");
        let objectUrl = LP.findOne({
          "url": qUrl
        });
        if (objectUrl) {
          $(linkPreview).find('.link-image').attr("src", objectUrl.image);
          $(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(objectUrl.title, 100));
          $(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(objectUrl.description, 200));
        } else {
          HTTP.call('GET', 'http://api.linkpreview.net/?key=59c881e95420219e6fdde211a88280424205b456ef721&q=' + qUrl, {}, function (error, response) {
            if (error) {
              console.log(error);
            } else {
              $(linkPreview).find('.link-image').attr("src", response.data.image);
              $(linkPreview).find('.link-title').text(UI._globalHelpers.limitPara(response.data.title, 100));
              $(linkPreview).find('.link-description').text(UI._globalHelpers.limitPara(response.data.description, 200));
              let lpData = {
                "url": qUrl,
                "image": response.data.image,
                "title": response.data.title,
                "description": response.data.description
              };
              Meteor.call('addLinkPreview', lpData, (result) => {
                console.log(result)
              });
            }
          });

        }
        $(linkPreview).removeClass('unloaded');
      }, 1000);
    })


    setTimeout(()  => {
      var slickThreadsList  = $(".list-comment.unloaded").filter(function() {
        if ($.trim($(this).html()) === "") {
          $(this).html('');
        }
        return $.trim($(this).html()) !== "";
      });

      $.each( slickThreadsList , function( key, value ) {
        $(value).owlCarousel({
          items: 2,
          autoWidth: true,
          loop: true,
          lazyLoad: true,
          dots: false
        });
        $(value).removeClass('unloaded');
      });

    }, 2000);
  },
  'click .prev-discussions': function(event, template) {
    event.preventDefault()
    let scrollValue = parseInt(localStorage.getItem("horizontalScrollValue"));
    if(scrollValue > 0) {
      localStorage.setItem("horizontalScrollValue", scrollValue - (478 * template.limitBy));
    }
    $('html, body').animate({
      scrollLeft: localStorage.getItem("horizontalScrollValue")
    }, 300);
  }

});
