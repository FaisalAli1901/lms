import { Template } from 'meteor/templating';

Template.threadContainer.onDestroyed(() => {
  Session.set('activeThread', '');
})

Template.threadContainer.helpers({
  getDiscussion() {
    Meteor.subscribe('discussions');
    let discussion_id = Router.current().params.did;
    if (discussion_id) {
      return Discussion.findOne({
        _id: discussion_id
      });
    }
  }
});

Template.commentForm.onCreated(() => {
  Meteor.subscribe('comments')
});

Template.threadContainer.onCreated(() => {
  Meteor.subscribe('threads');
  Meteor.subscribe('comments');
});

Template.commentList.onRendered(() => {
  setTimeout(() => {
    let commentsDiv = document.querySelector(".comments");
    commentsDiv.scrollTop = commentsDiv.scrollHeight;
  }, 500 );
});

Template.threadContainer.onRendered(() => {

  let discussion_id = Router.current().params.did;
      thread_id = Router.current().params.tid;

  if(thread_id) {
    $('.list-group-item[data-thread-id="' + thread_id + '"]').trigger('click');
  } else {
    $('.start-new-thread').trigger('click');
  }

  let showThreadPopover = $("#showThreadPopover").popover({
    "html": 'true',
    "placement": 'bottom',
    "trigger": 'hover',
    "container": 'body',
    "content": ``
  });

  showThreadPopover.on("show.bs.popover", function(e) {
    var popover = $(this)
      , w = popover.attr("data-width")
      , h = popover.attr("data-height")
      ;
    showThreadPopover.data("bs.popover").tip().css({width: w, height: h, position: 'fixed'});
  });

});

Template.threadForm.onRendered(() => {
  let thread_editor = $('.thread-summernote').summernote({
    placeholder: 'start typing',
    disableResizeEditor: false,
    height: 100,
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['insert', ['link', 'picture', 'video']]
    ],
    focus: true,
    callbacks: {
      onChange: function (contents, $editable) {
        let target_el = event.target,
          note_editor = $(target_el).closest('.note-editor'),
          summernote_input = note_editor.siblings('.summernote-left-article');

        summernote_input.val(contents);
      },
      onImageUpload: function (files, editor, welEditable) {
        for (var i = files.length - 1; i >= 0; i--) {
          sendFile(files[i], this);
        }

        function sendFile(file, el) {
          data = new FormData();
          data.append("file", file);
          FS.Utility.eachFile(event, function (file) {
            Images.insert(file, function (err, fileObj) {
              if (err) {
                console.log("error in inserting the image");
              } else {
                var imagesURL = "/cfs/files/images/" + fileObj._id;
                setTimeout(() => {
                  $(el).summernote('editor.insertImage', location.origin + imagesURL, '');
                  $('[name="coverPic"]').val(location.origin + imagesURL);
                }, 1000);
              }
            });
          });
        }
      }
    }
  });
})

Template.threadList.helpers({
  fetchThreads() {
    let discussion_id = Router.current().params.did;
    return Thread.find({"discussion_id": discussion_id}, {sort: {createdAt: -1}}).fetch();
  }
});

Template.commentList.helpers({
  isCurrentUser(author) {
    let currentUserId = Meteor.user()._id;
    if (currentUserId == author) {
      return "currentUser"
    }
  },
  fetchComments() {
    let thread_id = Session.get('activeThread');
    let comments = Comment.find({
      "thread_id": thread_id
    }).fetch();
    if (comments) {
      return comments;
    }
  },
  fetchThread() {
    let thread_id = Session.get('activeThread');
    return Thread.findOne(thread_id);
  },
  getParentComment(comment_id) {
		console.log('getParentComment', comment_id);
    if(comment_id) {
			debugger
			let temp = Comment.findOne(comment_id);
			console.log(temp);
			if (temp) {
				return temp
				
			}
    }
  }
});

Template.commentForm.helpers({
  getParentComment(comment_id) {
    if(comment_id) {
      return Comment.findOne(comment_id);
    }
  }
});

Template.threadList.events({
  'click .thread': (event) => {
    event.preventDefault();
    var el = event.currentTarget;
    $('.list-group-item').removeClass('active');
    el.closest('.list-group-item').classList.add('active');
    Session.set('activeThread', el.dataset.threadId);
    $("#newThreadForm").addClass("hidden");
  },
  'click .start-new-thread': (event) => {
    event.preventDefault();
    $('.list-group-item').removeClass('active');
    Session.set('activeThread', '');
    $("#newThreadForm").removeClass("hidden");
  }

});

Template.threadForm.events({
  'submit #newThreadForm': function (e, template) {
    e.preventDefault();
    let discussion_id = Router.current().params.did;
    let data = {
      "body": e.target.body.value,
      "discussion_id": discussion_id,
      "createdAt": Date.now(),
      "upVote": [],
      "downVote": [],
      "comments": []
    }
    if (Meteor.user() == null) {
      console.log("No signed in user");
    } else {
      data.author = Meteor.user()._id;
    }
    if($("#newThreadForm").find(".note-editable").text().trim() !== "") {
      Meteor.call('createThread', data, (error, result) => {
        $('#newThreadForm')[0].reset();
        $('.note-editable').html('');
        setTimeout(() => {
          $("li[data-thread-id='" + result + "']").trigger('click');
        }, 500);
        let user_id = Meteor.user()._id,
          discussion_id = Router.current().params.did;
        Meteor.call('addThreadtoDiscussion', discussion_id, result);
        Meteor.call('addUsertoActiveUsers', discussion_id, user_id);
      });
    }
  }
});

Template.commentForm.events({
  'submit #newCommentForm': function (e, template) {
    e.preventDefault();
    let data = {
      "body": e.target.body.value,
      "createdAt": Date.now(),
      "agree": [],
      "disagree": [],
      "replyTo": e.target.replyTo ? e.target.replyTo.value : ""
    }
    if (Meteor.user() == null) {
      console.log("No signed in user");
    } else {
      data.author = Meteor.user()._id;
    }
    data.thread_id = $('.list-group-item.thread.active').data('threadId');
    if (data.thread_id && $("#newCommentForm").find("textarea").val().trim() !== "") {
      Meteor.call('createComment', data, (error, result) => {
        $("#newCommentForm")[0].reset();
        $('#newCommentForm #parentComment, #newCommentForm .reply-parent-comment').remove();
        let user_id = Meteor.user()._id,
          discussion_id = Router.current().params.did;

        Meteor.call('addCommenttoThread', data.thread_id, result);
        Meteor.call('addUsertoActiveUsers', discussion_id, user_id);
        setTimeout(() => {
          let commentsDiv = document.querySelector(".comments");
          commentsDiv.scrollTop = commentsDiv.scrollHeight;
        }, 1000 );
      });
    }
  }
});

Template.commentList.events({
  'click .agreeComment': function (e, template) {
    e.preventDefault();
    let comment_id = $(e.currentTarget).data("commentId"),
        userId = Meteor.user()._id;

    Meteor.call('updateAgreeComment', comment_id, userId);
  },
  'click .disagreeComment': function (e, template) {
    e.preventDefault();
    let comment_id = $(e.currentTarget).data("commentId"),
        userId = Meteor.user()._id;

    Meteor.call('updateDisagreeComment', comment_id, userId);
  },
  'click .comment-options': function (e, template) {
    e.preventDefault();
    let jqEl = $(e.currentTarget);
    jqEl.toggleClass('active').siblings('.comment-buttons').toggleClass('hidden');
  },
  'click .reply-btn': function (e, template) {
    e.preventDefault();
    let parentCommentId = $(e.currentTarget).data("commentId");

    $('<input>').attr({
      type: 'hidden',
      id: 'parentComment',
      name: 'replyTo',
      value: parentCommentId
    }).appendTo('#newCommentForm');

    let parentComment = Comment.findOne(parentCommentId)
      parentCommentUser = Meteor.users.findOne({
        "_id": parentComment.author
      });

    $('#newCommentForm').prepend(`<div class="reply-bg reply-parent-comment well-sm"><p class="reply-to">Reply to ${parentCommentUser.username}</p>${parentComment.body}<img class="close-reply" src="/button-close.svg" alt="close reply button" /></div>`)
  }

});

$(document).on('click', '.close-reply',(e) => {
  $('#newCommentForm #parentComment, #newCommentForm .reply-parent-comment').remove();
});
