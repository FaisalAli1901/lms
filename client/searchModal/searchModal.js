import { Template } from 'meteor/templating';
import { Index, MinimongoEngine } from 'meteor/easy:search'

let template;

const ResourceIndex = new Index({
	collection: Resource,
	fields: ['title'],
	engine: new MinimongoEngine(),
});

const DiscussionIndex = new Index({
	collection: Discussion,
	fields: ['title'],
	engine: new MinimongoEngine(),
});

const UserIndex = new Index({
	collection: Meteor.users,
	fields: ['username'],
	engine: new MinimongoEngine(),
});

Template.searchModal.onCreated(() => {
	template = Template.instance();

	template.searchQuery = new ReactiveVar();
	template.searching = new ReactiveVar(false);
	
});

Template.searchModal.helpers({
	searching() {
		let searching = Template.instance().searching.get();
		console.log(searching);
		return searching;
	},
	query() {
		return Template.instance().searchQuery.get();
	},
	getData(){
		let query = Template.instance().searchQuery.get();
		let searchData;
		let routeData = Router.current().route.path()
		if(query){
			template.autorun(() => {
				searchData = UserIndex.search(query)
				if (routeData == '/library') {
					searchData = ResourceIndex.search(query)
					searchData = UserIndex.search(query)
				}else if (routeData == '/discussions') {
					searchData = DiscussionIndex.search(query)
					searchData = UserIndex.search(query)
				}
				
				searchData = searchData.fetch()
				setTimeout(() => {
					template.searching.set(false);
				}, 200)
			})
			return searchData
		}else{
			template.searching.set(false);
		}
	},
	getUser(){
		let query = Template.instance().searchQuery.get();
		let searchData;
		let routeData = Router.current().route.path()

		if (query) {
			template.autorun(() => {
				searchData = UserIndex.search(query)

				searchData = searchData.fetch()
				setTimeout(() => {
					template.searching.set(false);
				}, 200)
			})
			return searchData
		} else {
			template.searching.set(false);
		}
	},
	getTemplate(data){
		let routeData = Router.current().route.path()
		console.log(data);
		if (routeData == '/library') {
			return {
				temp: 'card1',
				data: data
			}
		} else if (routeData == '/discussions') {
			return {
				temp: 'cardDiscussion',
				data: data
			}
		}
	}
});

Template.searchModal.events({
	'click .dismis': (e) => {
		e.preventDefault();
		Modal.hide('searchModal');
	},
	'input .search-container': (e, t) => {
		let value = e.target.innerText.trim()
		if(value !== ''){
			template.searchQuery.set(value);
			template.searching.set(true);
		}

		if (value === ''){
			template.searchQuery.set(value);
		}
	}
});