import { Template } from 'meteor/templating';

Template.chatModel.helpers({
    getUsers: () =>  {
        Meteor.subscribe("users");
        Meteor.subscribe("presences");

        let userIds = Presences.find().map((presence) => { return presence.userId; });
        let onlineusers = Meteor.users.find({ _id: { $in: userIds, $ne: Meteor.userId() } }).fetch()
        console.log(onlineusers[0]);
        return onlineusers[0]
    }
});

Template.chatModel.events({
    'click .dismis': (e) => {
        e.preventDefault();
        Modal.hide('chatModel');
    },
    'click .model-dismiss': (e) => {
        let userId = e.target.id
        Modal.hide('chatModel');
    }
});