import { Template } from 'meteor/templating';

Template.analytics.events({
  'click .goto': (e) => {
    e.preventDefault();
    var scrollTo = $(e.currentTarget).attr("href");
    $('html, body').animate({
        scrollTop: $(scrollTo).offset().top - 140
    }, 800, function () {
        window.location.hash = scrollTo;
    });
  }
});
