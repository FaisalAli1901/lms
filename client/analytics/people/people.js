import { Template } from 'meteor/templating';

Template.people.onCreated(() => {
  google.charts.load('current', { 'packages': ['corechart'] });
});

Template.people.onRendered(() => {

  /* Draw line Chart */
  function drawLineChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Sales', 'Expenses'],
      ['2004',  1000,      400],
      ['2005',  1170,      460],
      ['2006',  660,       1120],
      ['2007',  1030,      540]
    ]);

    var options = {
      title: 'Company Performance',
      titleTextStyle: {
        fontSize: 24,
        color: '#5e696d',
        bold: true,
        italic: false
      },
      curveType: 'function',
      backgroundColor: "#2b373d",
      legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('lineChart'));

    chart.draw(data, options);
  }

  google.charts.setOnLoadCallback(drawLineChart);


  /* Draw Donut Chart */
  function drawDonutChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work',     11],
      ['Eat',      2],
      ['Commute',  2],
      ['Watch TV', 2],
      ['Sleep',    7]
    ]);

    var options = {
      title: 'Age Distribution',
      titleTextStyle: {
        fontSize: 24,
        color: '#5e696d',
        bold: true,
        italic: false
      },
      pieHole: 0.7,
      pieSliceBorderColor: 'none',
      backgroundColor: "#2b373d",
      legend: {
        position: 'bottom'
      }
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutChart'));
    chart.draw(data, options);
  }

  google.charts.setOnLoadCallback(drawDonutChart);


  /* Draw Bubble Chart */
  function drawBubbleChart() {

    var data = google.visualization.arrayToDataTable([
      ['ID', 'Life Expectancy', 'Fertility Rate', 'Region', 'Population'],
      ['CAN', 80.66, 1.67, 'North America', 33739900],
      ['DEU', 79.84, 1.36, 'Europe', 81902307],
      ['DNK', 78.6, 1.84, 'Europe', 5523095],
      ['EGY', 72.73, 2.78, 'Middle East', 79716203],
      ['GBR', 80.05, 2, 'Europe', 61801570],
      ['IRN', 72.49, 1.7, 'Middle East', 73137148],
      ['IRQ', 68.09, 4.77, 'Middle East', 31090763],
      ['ISR', 81.55, 2.96, 'Middle East', 7485600],
      ['RUS', 68.6, 1.54, 'Europe', 141850000],
      ['USA', 78.09, 2.05, 'North America', 307007000]
    ]);

    var options = {
      title: 'Top Influencers',
      titleTextStyle: {
        fontSize: 21,
        color: '#a0b2b8',
        bold: false,
        italic: false
      },
      chartArea: {left:70, top:100, bottom:60, right: 50, width:'100%', height:'75%'},
      hAxis: {
        title: 'Croud Pulled',
        titleTextStyle: {
          fontSize: 21,
          color: '#5a666a',
          bold: true,
          italic: false
        },
        baseline: 1,
        baselineColor: '#191E21',
        gridlines: {
          color: "#2b373d"
        }
      },
      vAxis: {
        title: 'Opinions Changed',
        titleTextStyle: {
          fontSize: 21,
          color: '#5a666a',
          bold: true,
          italic: false
        },
        baseline: 1,
        baselineColor: '#191E21',
        gridlines: {
          color: "#2b373d"
        }
      },
      axisTitlesPosition: 'out',
      bubble: {
        textStyle: {
          fontSize: 12,
          color: 'black',
          bold: true,
          auraColor: 'white'
        }
      },
      legend: {
        position: 'none'
      },
      backgroundColor: "#2b373d"
    };

    var chart = new google.visualization.BubbleChart(document.getElementById('bubbleChart'));
    var chart2 = new google.visualization.BubbleChart(document.getElementById('bubbleChart2'));
    chart.draw(data, options);
    chart2.draw(data, options);
  }

  google.charts.setOnLoadCallback(drawBubbleChart);
});