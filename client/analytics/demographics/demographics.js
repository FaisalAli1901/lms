import { Template } from 'meteor/templating';

Template.demographics.onCreated(() => {
	google.charts.load('current', { 'packages': ['corechart'] }, {
		'packages': ['geochart'],
		'mapsApiKey': 'AIzaSyAUPkx73vOXQl-Q0UmcDXy2XuWzqDU78M8'
	});
});

Template.demographics.onRendered(() => {

	/* Draw donut Chart */
	function drawDonutChart30() {
		var data = google.visualization.arrayToDataTable([
			['gender', 'Percentage'],
			['', 50],
			['', 50]
		]);

		var options = {
			width: 297,
			height: 150,
			pieHole: 0.5,
			legend: 'none',
			pieSliceText: 'none',
			pieStartAngle: -90,
			tooltip: { trigger: 'none' },
			chartArea: { top: 50, width: "100%", height: "100%" },
			slices: {
				0: { color: 'yellow' },
				1: { color: 'transparent' }
			}
		};

		var chart = new google.visualization.PieChart(document.getElementById('age-gender-donut-chart-30'));
		chart.draw(data, options);
	}

	/* Draw donut Chart */
	function drawDonutChart50() {
		var data = google.visualization.arrayToDataTable([
			['gender', 'Percentage'],
			['male', 76],
			['female', 24],
			['hide', 100]
		]);

		var options = {
			width: 297,
			height: 150,
			pieHole: 0.5,
			legend: 'none',
			pieSliceText: 'none',
			pieStartAngle: -90,
			// tooltip: { trigger: 'none' },
			slices: {
				0: { color: 'yellow' },
				1: { color: 'blue'},
				2: { color: 'transparent' }
			}
		};

		var chart = new google.visualization.PieChart(document.getElementById('age-gender-donut-chart-50'));
		chart.draw(data, options);
	}

	/* Draw donut Chart */
	function drawDonutChart70() {
		var data = google.visualization.arrayToDataTable([
			['Pac Man', 'Percentage'],
			['', 50],
			['', 50]
		]);

		var options = {
			width: 297,
			height: 150,
			pieHole: 0.5,
			legend: 'none',
			pieSliceText: 'none',
			pieStartAngle: -90,
			tooltip: { trigger: 'none' },
			slices: {
				0: { color: 'yellow' },
				1: { color: 'transparent' }
			}
		};

		var chart = new google.visualization.PieChart(document.getElementById('age-gender-donut-chart-70'));
		chart.draw(data, options);
	}

	/* Draw donut Chart */
	function drawDonutChart90() {
		var data = google.visualization.arrayToDataTable([
			['Pac Man', 'Percentage'],
			['', 50],
			['', 50]
		]);

		var options = {
			width: 297,
			height: 150,
			pieHole: 0.5,
			legend: 'none',
			pieSliceText: 'none',
			pieStartAngle: -90,
			tooltip: { trigger: 'none' },
			slices: {
				0: { color: 'yellow' },
				1: { color: 'transparent' }
			}
		};

		var chart = new google.visualization.PieChart(document.getElementById('age-gender-donut-chart-90'));
		chart.draw(data, options);
	}

	google.charts.setOnLoadCallback(drawDonutChart30);
	google.charts.setOnLoadCallback(drawDonutChart50);
	google.charts.setOnLoadCallback(drawDonutChart70);
	google.charts.setOnLoadCallback(drawDonutChart90);

	// Geo Chart
	function drawRegionsMap() {
		var data = google.visualization.arrayToDataTable([
			['Country', 'Popularity'],
			['Germany', 200],
			['United States', 300],
			['Brazil', 400],
			['Canada', 500],
			['France', 600],
			['RU', 700]
		]);

		var options = {
			width: "100%",
			height: 500,
			backgroundColor: 'transparent',
			legend: {
				position: 'none'
			}
		};

		var chart = new google.visualization.GeoChart(document.getElementById('geo-chart'));

		chart.draw(data, options);
	}

	google.charts.setOnLoadCallback(drawRegionsMap);
})