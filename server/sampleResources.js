import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
	SampleResources.remove({})

	// SampleResources.insert({
	// 	product: 'yes peace',
	// 	coverPic: '/home/resource-card1.png',
	// 	title: 'Title of this resource will go here like this',
	// 	description: 'This is purely a representation, again this is a representation and not a real content and not a real content.',
	// 	views: 234,
	// 	pages: 5,
	// 	stars: '4/5',
	// 	users: [
	// 		{ userId: 1 },
	// 		{ userId: 2 },
	// 		{ userId: 3 },
	// 		{ userId: 4 },
	// 		{ userId: 5 }
	// 	],
	// 	activeUser: '/home/discussion-comment.png',
	// 	activeUserName: 'Alex Hayman'
	// });

	// SampleResources.insert({
	// 	product: 'LIBRE',
	// 	coverPic: '/home/resource-card1.png',
	// 	title: 'Title of this cool resource will float here',
	// 	description: 'This is purely a representation, again this is a representation and not a real content.',
	// 	pages: 5,
	// 	views: 234,
	// 	stars: '4/5',
	// 	users: [
	// 		{ userId: 1 },
	// 		{ userId: 2 },
	// 		{ userId: 3 },
	// 		{ userId: 4 },
	// 		{ userId: 5 }
	// 	],
	// 	activeUser: '/home/discussion-comment.png',
	// 	activeUserName: 'Alex Hayman'
	// })
	
	// SampleResources.insert({
	// 	product: 'C3',
	// 	coverPic: '/home/card-3.png',
	// 	title: 'Title of this awesome & cool resource is here',
	// 	description: 'This is purely a representation, again this is a representation.',
	// 	pages: 5,
	// 	views: 234,
	// 	stars: '4/5',
	// 	users: [
	// 		{ userId: 1 },
	// 		{ userId: 2 },
	// 		{ userId: 3 },
	// 		{ userId: 4 },
	// 		{ userId: 5 }
	// 	],
	// 	activeUser: '/home/discussion-comment.png',
	// 	activeUserName: 'Alex Hayman'
	// })

	// SampleResources.insert({
	// 	product: 'Youth Leaders',
	// 	coverPic: '/home/card-4.png',
	// 	title: 'Title of this awesome & cool resource is here',
	// 	description: `This is purely a representation, again this is a representation and not a real content and not a real content. This is purely
	// 			a representation, again this is a representation and not a real content and not a real content.`,
	// 	pages: 5,
	// 	views: 234,
	// 	stars: '4/5',
	// 	users: [
	// 		{ userId: 1 },
	// 		{ userId: 2 },
	// 		{ userId: 3 },
	// 		{ userId: 4 },
	// 		{ userId: 5 }
	// 	],
	// 	activeUser: '/home/discussion-comment.png',
	// 	activeUserName: 'Alex Hayman'
	// })

	// SampleResources.insert({
	// 	product: 'Youth Leaders',
	// 	coverPic: '/home/resource-card1.png',
	// 	title: 'Title of this insane resource will go here like this',
	// 	description: `This is purely a representation, again this is a representation. This is purely a representation, again`,
	// 	pages: 5,
	// 	views: 234,
	// 	stars: '4/5',
	// 	users: [
	// 		{ userId: 1 },
	// 		{ userId: 2 },
	// 		{ userId: 3 },
	// 		{ userId: 4 },
	// 		{ userId: 5 }
	// 	],
	// 	activeUser: '/home/discussion-comment.png',
	// 	activeUserName: 'Alex Hayman'
	// })

	// SampleResources.insert({
	// 	product: 'LIBRE',
	// 	coverPic: '/home/resource-card1.png',
	// 	title: 'Title of this cool resource will float here',
	// 	description: 'This is purely a representation, again this is a representation and not a real content.',
	// 	pages: 5,
	// 	views: 234,
	// 	stars: '4/5',
	// 	users: [
	// 		{ userId: 1 },
	// 		{ userId: 2 },
	// 		{ userId: 3 },
	// 		{ userId: 4 },
	// 		{ userId: 5 }
	// 	],
	// 	activeUser: '/home/discussion-comment.png',
	// 	activeUserName: 'Alex Hayman'
	// })
	SampleResources.insert({
		program: 'LIBRE',
		coverPic: '/home/resource-card1.png',
		title: 'Title of this cool resource will float here',
		tags: ["tag1", "tag2"],
		description: 'This is purely a representation, again this is a representation and not a real content.',
		pages: [1, 2, 3],
		views: 234,
		stars: 4,
		author: 'email'
	})
});