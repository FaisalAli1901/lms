import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random'

Meteor.startup(() => {
    UsersId.remove({});
    UsersOnline.remove({});
});

if (Meteor.isServer) {
    Meteor.publish('presences',  () => {
        return Presences.find({}, { userId: true });
    });
    Meteor.publish("users",  () => {
        return Meteor.users.find();
    });
    Meteor.publish('currentChat', () => {
        return CurrentChat.find({}, { name: true });
    });
}