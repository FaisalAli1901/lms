import {
  Meteor
} from 'meteor/meteor';

Meteor.startup(() => {

});

Meteor.methods({
  fetchThreads: () => {
    return Thread.find().fetch();
  },
  createThread: (data) => {
    return Thread.insert(data);
  },
  deleteThread: (id) => {
    Thread.remove({
      _id: id
    });
  },
  updateThread: (id, data) => {
    return Thread.update(id, {
      $set: data
    });
  },
  updateUpVote: (thread_id, data) => {
    if(Thread.find({ _id:thread_id, upVote: { $in : [data]} }).count() > 0) {
      return Thread.update(thread_id, {
        $pull: {
          upVote: data
        }
      });
  } else {
    return Thread.update(thread_id, {
      $addToSet: {
        upVote: data
      }
    });
  }
    return Thread.update(thread_id, {
      $addToSet: {
        'upVote': 1
      }
    });
  },
  updateDownVote: (thread_id, data) => {
    if(Thread.find({ _id:thread_id, downVote: { $in : [data]} }).count() > 0) {
      return Thread.update(thread_id, {
        $pull: {
          downVote: data
        }
      });
  } else {
    return Thread.update(thread_id, {
      $addToSet: {
        downVote: data
      }
    });
  }
    return Thread.update(thread_id, {
      $addToSet: {
        'downVote': 1
      }
    });
  },
  addCommenttoThread: (thread_id, data) => {
    return Thread.update(thread_id, {
      $push: {
        comments: data
      }
    });
  }
});

Meteor.publish("threads", () => {
  return Thread.find();
});
