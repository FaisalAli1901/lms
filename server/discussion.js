import { Meteor } from 'meteor/meteor';
import { Index, MinimongoEngine } from 'meteor/easy:search'

Meteor.startup(() => {

});

Meteor.methods({
  createDiscussion: (data) => {
    return Discussion.insert(data);
  },
  deleteDiscussion: (id) => {
    Discussion.remove({
      _id: id
    });
  },
  addThreadtoDiscussion: (discussion_id, data) => {
    return Discussion.update(discussion_id, {
      $push: {
        threads: data
      }
    });
  },
  updateDiscussion: (id, data) => {
    return Discussion.update(id, {
      $set: data
    });
  },
  updateAgree: (discussion_id, data) => {
    if(Discussion.find({ _id:discussion_id, agree: { $in : [data]} }).count() > 0) {
      return Discussion.update(discussion_id, {
        $pull: {
          agree: data
        }
      });
  } else {
    return Discussion.update(discussion_id, {
      $addToSet: {
        agree: data
      }
    });
  }
  },
  updateDisagree: (discussion_id, data) => {
    if(Discussion.find({ _id:discussion_id, disagree: { $in : [data]} }).count() > 0) {
      return Discussion.update(discussion_id, {
        $pull: {
          disagree: data
        }
      });
  } else {
    return Discussion.update(discussion_id, {
      $addToSet: {
        disagree: data
      }
    });
  }
  },
  updateLike: (discussion_id, data) => {
    if(Discussion.find({ _id:discussion_id, like: { $in : [data]} }).count() > 0) {
        return Discussion.update(discussion_id, {
          $pull: {
            like: data
          }
        });
    } else {
      return Discussion.update(discussion_id, {
        $addToSet: {
          like: data
        }
      });
    }
  },
  updateBookmark: (discussion_id, data) => {
    if(Discussion.find({ _id:discussion_id, bookmark: { $in : [data]} }).count() > 0) {
      return Discussion.update(discussion_id, {
        $pull: {
          bookmark: data
        }
      });
  } else {
    return Discussion.update(discussion_id, {
      $addToSet: {
        bookmark: data
      }
    });
  }
  },
  addUsertoActiveUsers: (discussion_id, data) => {
    return Discussion.update(discussion_id, {
      $addToSet: {
        users: data
      }
    });
  },
  filterMostUpvotedThread: (discussion_id) => {
    let thread = Thread.aggregate(
      [
        { $match: { "discussion_id": discussion_id }},
        { "$unwind": {
          "path": "$upVote",
          "preserveNullAndEmptyArrays": true
        } },
        { $group : { _id : "$_id", len : { $sum : 1 } } },
        { $sort : { len : -1 } },
        { $limit : 1 }
      ]
    )
    return Thread.findOne({ _id: thread[0]._id });
  },
  filterMostCommentedThread: (discussion_id) => {
    let thread = Thread.aggregate(
      [ { $match: { "discussion_id": discussion_id }},
        { "$unwind": {
          "path": "$comments",
          "preserveNullAndEmptyArrays": true
        } },
        { $group : { _id : "$_id", len : { $sum : 1 } } },
        { $sort : { len : -1 } },
        { $limit : 1 }
      ]
    )
    return Thread.findOne({ _id: thread[0]._id });
  },
  addLinkPreview: (lpData) => {
    return LP.insert(lpData);
  }

});

const DiscussionIndex = new Index({
  collection: Discussion,
  fields: ['title'],
  engine: new MinimongoEngine(),
});


Meteor.publish('discussionList', function(limit) {
  check(limit, Number);
  Counts.publish(this, 'discussionList', Discussion.list(), { noReady: true });

  // mimic latency
  Meteor._sleepForMs(1000);

  return Discussion.list({ limit: limit });
});

Meteor.publish("discussions", () => {
  return Discussion.find({}, {sort: {createdAt: -1}, limit: 6});
});

Meteor.publish('discussion', function (discussionId) {
  check(discussionId, String);
  return [
    Discussion.find({ _id: discussionId })
  ];
});

Meteor.publish("linkPreviews", () => {
  return LP.find();
});
