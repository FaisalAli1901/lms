import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {

});

Meteor.methods({
  fetchComments: () => {
    return Comment.find().fetch();
  },
  createComment: (data) => {
    return Comment.insert(data);
  },
  deleteComment: (id) => {
    Comment.remove({ _id: id });
  },
  updateComment: (id, data) => {
		console.log('updateComment', data);
    return Comment.update(id, {
      $set: data
    });
  },
  updateAgreeComment: (comment_id, data) => {
    if(Comment.find({ _id:comment_id, agree: { $in : [data]} }).count() > 0) {
      return Comment.update(comment_id, {
        $pull: {
          agree: data
        }
      });
  } else {
    return Comment.update(comment_id, {
      $addToSet: {
        agree: data
      }
    });
  }
  },
  updateDisagreeComment: (comment_id, data) => {
    if(Comment.find({ _id:comment_id, disagree: { $in : [data]} }).count() > 0) {
      return Comment.update(comment_id, {
        $pull: {
          disagree: data
        }
      });
  } else {
    return Comment.update(comment_id, {
      $addToSet: {
        disagree: data
      }
    });
  }
  }
});

Meteor.publish("comments", () => {
  return Comment.find();
});
