import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random'

Meteor.startup(() => {
	Debate.remove({});

	Discussion.remove({"_id": "1"});

	Discussion.insert({
			"_id" : "1",
			"title" : "Title of the debate will sit here",
			"body" : "Description of the debate will go here. Read this article.\n\t\t\t\t\t\tPeople are sharing this widely. I always believed that UBER is the most evil company we have right now.\n\t\t\t\t\t\tI have this strong urge to delete my uber app right now. But will this help? Read this article.\n\t\t\t\t\t\tPeople are sharing this widely. I always believed that UBER is the most evil company we have right now.\n\t\t\t\t\t\tI have this strong urge to delete my uber app right now. But will this help?",
			"threads" : [],
			"agree" : [],
			"disagree" : [],
			"like" : [],
			"bookmark" : [],
			"users" : [],
			"author" : "id8PSvbYkNjjmvMkt",
			"link" : "http://blog.ubi.com/games-for-learning-summit/",
			"createdAt" : 1507523958824.0
	}	)


	Debate.insert({
		_id: '1',
		title: 'Title of the debate will sit here',
		description: `Description of the debate will go here. Read this article.
					People are sharing this widely. I always believed that UBER is the most evil company we have right now.
					I have this strong urge to delete my uber app right now. But will this help? Read this article.
					People are sharing this widely. I always believed that UBER is the most evil company we have right now.
					I have this strong urge to delete my uber app right now. But will this help?`,
		started: true,
		days: '6',
		ended: false,
		agree: {
			userId: '1',
			debaterPic: "/debate/participant2.png",
			totalAgree: 46,
			statement: `This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company.`,
			tags: ['compassion', 'health'],
			detailStatement: `This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right.
This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. `,
			conclusion: `This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right.
This is a fake comment is this. `
		},
		disagree: {
			userId: '2',
			debaterPic: "/debate/participant1.png",
			totalDisagree: 54,
			statement: `This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company.`,
			tags: ['compassion', 'health'],
			detailStatement: `This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right.
This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. `,
			conclusion: `This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right.
This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. This is a fake comment is this. Read this article. People are sharing this widely. I always believed that UBER is the most evil company we have right. `
		},
		arguments: 56,
		views: 50000,
		opinionChanged: 10000,
		questionRaised: 890,
		conversations: [
			{
				id: Random.id(12),
				userId: '2',
				debatorMsg: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.`,
				createdAt: 1508841900,
				attachment: '',
				applause: 349,
				activeUsers: [
					{ userId: 'someId' }
				],
				comments: [
					{
						id: Random.id(12),
						userId: 'id',
						message: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s. FaisalAli`,
						type: 'seeks clarification',
						likes: 12,
						dislikes: 34,
						applause: 134,
						createdAt: 1508841900,
					},
					{
						id: Random.id(12),
						userId: 'id',
						message: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.`,
						type: 'Post counter Facts',
						likes: 12,
						dislikes: 34,
						applause: 80,
						createdAt: 1508841900,
					}
				]
			},
			{
				id: Random.id(12),
				userId: '1',
				debatorMsg: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.`,
				createdAt: 1508841900,
				attachment: '',
				applause: 349,
				activeUsers: [
					{ userId: 'someId' }
				],
				comments: [
					{
						id: Random.id(12),
						userId: 'id',
						message: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.`,
						type: 'seeks clarification',
						likes: 12,
						dislikes: 30,
						applause: 134,
						createdAt: 1508841900,
					}
				]
			},
			{
				id: Random.id(12),
				userId: '2',
				debatorMsg: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.`,
				createdAt: 1508841900,
				attachment: '',
				applause: 349,
				activeUsers: [
					{ userId: 'someId' }
				],
				comments: [
					{
						id: Random.id(12),
						userId: 'id',
						message: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.`,
						type: 'seeks clarification',
						likes: 12,
						dislikes: 34,
						applause: 120,
						createdAt: 1508841900,
					}
				]
			}
		],
		participantTake: [

		]
	})
});

Meteor.methods({
	editUserInput(type, statement, tags, detailStatement) {
		let temp = Debate.findOne({});
		let id = temp._id
		temp[type].statement = statement
		temp[type].tags = tags
		temp[type].detailStatement = detailStatement

		if(type === 'agree' && temp){
			Debate.update(id, {$set:{
				agree: temp.agree
			}});
		} else if(type === 'disagree' && temp){
			Debate.update(id, {
				$set: {
					disagree: temp.disagree
				}
			});
		}

	},
	createUserInput(type, data){
		let temp = Debate.findOne({});
		let id = temp._id

		console.log(data);
		temp[type] = data
		console.log(temp);

		if (type === 'agree' && temp) {
			Debate.update(id, {
				$set: {
					agree: temp.agree
				}
			});
		} else if (type === 'disagree' && temp) {
			Debate.update(id, {
				$set: {
					disagree: temp.disagree
				}
			});
		}
	},
	addConversation(message, userId) {
		let temp = Debate.findOne({});
		let id = temp._id
		let data = {
			id: Random.id(12),
			userId: userId,
			debatorMsg: message,
			createdAt: Date.now(),
			attachment: '',
			applause: 349,
			thread_id: "",
			activeUsers: [
				{ userId: 'someId' }
			]
		}
		temp.conversations.push(data)
		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	addThreadAndComments(debateId, threadId, message) {
		let temp = Debate.findOne({_id: debateId});
		let id = temp._id

		 temp.conversations.forEach((data) => {
				if(data.id == threadId) {
					let threadData = {
						"body": data.debatorMsg,
						"discussion_id": "1",
						"createdAt": Date.now(),
						"upVote": [],
						"downVote": [],
						"comments": [],
						"author": this.userId
					}
					
					if(data.thread_id) {

						let commentData = {
							"body": message,
							"createdAt": Date.now(),
							"agree": [],
							"disagree": [],
							"replyTo": "",
							"author": this.userId,
							"thread_id": data.thread_id
						}
						console.log('called if');
						let cId = Meteor.call('createComment', commentData);

						data.comments.forEach((comment) => {
							if (comment.message === message) {
								comment.commentId = cId
								Debate.update(id, {
									$set: {
										conversations: temp.conversations
									}
								});
							}
						})

					} else {
						let tId = Meteor.call('createThread', threadData);
						data.thread_id = tId;

						Debate.update(id, {
							$set: {
								conversations: temp.conversations
							}
						});

						let commentData = {
							"body": message,
							"createdAt": Date.now(),
							"agree": [],
							"disagree": [],
							"replyTo": "",
							"author": this.userId,
							"thread_id": tId
						}

						let cId = Meteor.call('createComment', commentData);
						console.log('CID', cId );

						data.comments.forEach((comment) => {
							if(comment.message === message){
								comment.commentId = cId
								Debate.update(id, {
									$set: {
										conversations: temp.conversations
									}
								});
							}
						})
					}
				}
		 })
	},
	replyToComment(threadId, commentId, msg){
		console.log(commentId);
		let commentData = {
			"body": msg,
			"createdAt": Date.now(),
			"agree": [],
			"disagree": [],
			"replyTo": commentId,
			"author": this.userId,
			"thread_id": threadId
		}

		Meteor.call('createComment', commentData);
	},
	addParticipantTake(Id, userId, takePoint, takeMsg){
		let temp = Debate.findOne({_id: Id});
		let id = temp._id
		let data = {
			userId,
			takePoint,
			takeMsg
		}
		temp.participantTake.push(data)
		Debate.update(id, {
			$set: {
				participantTake: temp.participantTake
			}
		});
	},
	editParticipantTake(Id, userId, takePoint, takeMsg){
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id
		temp.participantTake.forEach((data) => {
			if(data.userId === userId){
				data.takePoint = takePoint,
				data.takeMsg = takeMsg
			}
		});
		Debate.update(id, {
			$set: {
				participantTake: temp.participantTake
			}
		});
	},
	addFacts(Id, convId, link, msg, type){
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id
		console.log(msg, type);
		temp.conversations.forEach((data) => {
			if(data.id === convId){
				if (link) {
					let comment = {
						link,
						type,
						userId: this.userId,
						message: msg,
						likes: 0,
						dislikes: 0,
						createdAt: Date.now(),
						applause: 0,
						id: Random.id(12),
						commentId: ''
					}
					if(data.comments){
						data.comments.push(comment)
					}else{
						let comments = []
						comments.push(comment)
						data['comments'] = comments
					}
				} else {
					let comment = {
						type,
						userId: this.userId,
						message: msg,
						likes: 0,
						dislikes: 0,
						createdAt: Date.now(),
						applause: 0,
						id: Random.id(12),
						commentId: ''
					}
					if (data.comments) {
						data.comments.push(comment)
					} else {
						let comments = []
						comments.push(comment)
						data['comments'] = comments
					}
				}
			}
		})

		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	increApplauseDebator(Id, convId){
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id

		temp.conversations.forEach((data) => {
			if (convId === data.id) {
				data.applause = data.applause + 1
			}
		})
		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	increApplauseUser(Id, convId, commentId) {
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id

		temp.conversations.forEach((data) => {
			if (convId === data.id) {
				data.comments.forEach((comment) => {
					if (commentId === comment.id) {
						comment.applause = comment.applause + 1
					}
				})
			}
		})
		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	like(Id, convId, commentId) {
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id

		temp.conversations.forEach((data) => {
			if (convId === data.id) {
				data.comments.forEach((comment) => {
					if (commentId === comment.id) {
						comment.likes = comment.likes + 1
					}
				})
			}
		})
		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	dislike(Id, convId, commentId) {
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id

		temp.conversations.forEach((data) => {
			if (convId === data.id) {
				data.comments.forEach((comment) => {
					if (commentId === comment.id) {
						comment.dislikes = comment.dislikes + 1
					}
				})
			}
		})
		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	toggleStart(Id){
		let temp = Debate.findOne({ _id: Id });
		let id = temp._id

		temp.started = !temp.started

		Debate.update(id, {
			$set: {
				started: temp.started
			}
		});
	},
	postReply(debateId, convId, commentId, link, msg){
		let temp = Debate.findOne({ _id: debateId });
		let id = temp._id

		temp.conversations.forEach((data) => {
			if (data.id === convId) {
				data.comments.forEach((comment) => {
					if (comment.id == commentId) {
						let replies = {
							link,
							userId: this.userId,
							message: msg,
							likes: 0,
							dislikes: 0,
							createdAt: Date.now(),
							applause: 0,
							id: Random.id(12)
						}
						if (comment.replies) {
							comment.replies.push(replies)
						} else {
							let reply = []
							reply.push(replies)
							comment['replies'] = reply
						}
					}
				})
			}
		})

		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	},
	editReply(debateId, convId, commentId, link, msg){
		let temp = Debate.findOne({ _id: debateId });
		let id = temp._id

		temp.conversations.forEach((data) => {
			if (data.id === convId) {
				data.comments.forEach((comment) => {
					if (comment.id == commentId) {
						comment.replies.forEach((reply) => {
							if (reply.userId === this.userId) {
								reply.link = link
								reply.message = msg
							}
						})
					}
				})
			}
		})

		Debate.update(id, {
			$set: {
				conversations: temp.conversations
			}
		});
	}
});

Meteor.publish("debate", () => {
  return Debate.find();
});