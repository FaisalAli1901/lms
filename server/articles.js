import { Meteor } from 'meteor/meteor';

Meteor.methods({
  createArticle: (data) => {
    return Articles.insert(data);
  },
  deleteArticle: (id) => {
    Articles.remove({
      _id: id
    });
  },
  updateArticle: (id, data) => {
    return Articles.update(id, {
      $set: data
    });
  }
});

Meteor.publish("articles", () => {
  return Articles.find();
});