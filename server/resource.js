import { Meteor } from 'meteor/meteor';
import { Index, MinimongoEngine } from 'meteor/easy:search'

Meteor.startup(() => {
  Resource._ensureIndex({
    title: 1
  });
});

Meteor.methods({
  createResource: (data) => {
    return Resource.insert(data);
  },
  addPagetoResource: (resource_id, data) => {
    return Resource.update(resource_id, {
      $push: {
        pages: data
      }
    });
  },
  deletePageFromResource: (resource_id, page) => {
    return Resource.update(resource_id, {
      $pull: {
        pages: { id: page.id }
      }
    });
  },
  updateResourceLike: (resourceId, data) => {
    if(Resource.find({ _id: resourceId, like: { $in : [data]} }).count() > 0) {
        return Resource.update(resourceId, {
          $pull: {
            like: data
          }
        });
    } else {
      return Resource.update(resourceId, {
        $addToSet: {
          like: data
        }
      });
    }
  },
  updateResourceBookmark: (resourceId, data) => {
    if(Resource.find({ _id:resourceId, bookmark: { $in : [data]} }).count() > 0) {
      return Resource.update(resourceId, {
        $pull: {
          bookmark: data
        }
      });
  } else {
    return Resource.update(resourceId, {
      $addToSet: {
        bookmark: data
      }
    });
  }
  },
});

const ResourceIndex = new Index({
  collection: Resource,
  fields: ['title'],
  engine: new MinimongoEngine(),
});

Meteor.publish('resourceList', function(limit) {
  check(limit, Number);
  Counts.publish(this, 'resourcesList', Resource.list(), { noReady: true });

  // mimic latency
  Meteor._sleepForMs(1000);

  return Resource.list({ limit: limit });
});

Meteor.publish("resources", () => {
  return Resource.find({}, {sort: {createdAt: -1}, limit: 6});
});
