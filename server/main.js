import { Meteor } from 'meteor/meteor';
import { Index, MinimongoEngine } from 'meteor/easy:search'

Meteor.startup(() => {
  CurrentTemplate.remove({});
  TemplateCollab.remove({});
  TemplateCollab2.remove({});
  SingleCanvas.remove({});
  CurrentChat.remove({});

  CurrentChat.insert({
    name: ""
  })

  CurrentTemplate.insert({
    id: "",
    type: ""
  })

  // process.env.ROOT_URL = 'http://collectivehumanintelligence.org/';
  process.env.MAIL_URL = "smtp://postmaster@sandboxd8009e619ae3490baa0860fb763a1eeb.mailgun.org:ff1b7032016deda789c2e5aa7b2c6d42@smtp.mailgun.org:587";

});


Meteor.publish("currentTemplate", () => {
  return CurrentTemplate.find();
});

Meteor.methods({
  updateCurrentTemplate: (type, id) => {
    let temp = CurrentTemplate.findOne();
    let template_id = temp._id;
    CurrentTemplate.update(template_id, {
      $set: {
        type: type,
        id: id
      }
    });
  },
  updateCurrentChat: (chat) => {
    let temp = CurrentChat.findOne();
    let id = temp._id

    CurrentChat.update(id, {
      $set: {
        name: chat
      }
    });
  },
  clearSingleCanvas() {
    SingleCanvas.remove({})
  }
});

const UserIndex = new Index({
	collection: Meteor.users,
	fields: ['username'],
	engine: new MinimongoEngine(),
});