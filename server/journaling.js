import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {

});

Meteor.methods({
  createJournaling: (data) => {
    return Journaling.insert(data);
  },
  deleteJournaling: (id) => {
    Journaling.remove({
      _id: id
    });
  },
  updateJournaling: (id, data) => {
    return Journaling.update(id, {
      $set: data
    });
  }
});

Meteor.publish("journaling", () => {
  return Journaling.find();
});