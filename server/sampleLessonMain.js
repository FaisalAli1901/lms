import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
    BotState.remove({})
    BotState.insert({ "state": ["start", "Let's start"] })
    Playlist.remove({})
    Playlist.insert({
        "nodes": [{
                "type": "bot",
                "name": "",
                "id": "xyz",
                // "template": "/bot"
            },
            {
                "type": "article",
                "name": "",
                "id": "xyz",
                // "template": "/bot"
            }
        ]
    })
    Conversation.remove({})

    Conversation.insert({
        "conversationId": "xyz",
        "states": {
            "start": {
                "response": "Hi!, I am Prof. Mohanan and in this session I am going to help you understand more about what constitutes as violence. Let's start! <br> Try and complete the following: <br> Violence is...<br>",

                "userStates": {
                    "hits": ["someone attacks someone", "hits", "beats"],
                    "none": ["do not know"]
                }
            },

            "hits": {
                "response": `Ok, so you classify physical abuse as violence, do you think the following is violence?<br><iframe src="http://www.youtube.com/embed/W7qWa52k-nE"
   width="560" height="315" frameborder="0" allowfullscreen></iframe>`,
                "userStates": {
                    "hitsYes": ["yes"],
                    "hitsNo": ["no"]
                }
            },
            "none": {
                "userStates": {
                    "endabuses": ["bye"]
                },
                "response": "Ok, now say bye to end"

            },
            "hitsYes": {
                "response": "Ok, so why do you think playing sports is violent?",
                "userStates": {
                    "endabuses": ["bye"]
                }

            },
            "hitsNo": {
                "response": "Ok, so why do you think playing sports is not violent?",
                "userStates": {
                    "endabuses": ["bye"]
                }
            },
            "endgreetings": {
                "parent": "greetings",
                "response": "Cant enter more: end greetings"
            },
            "endabuses": {
                "parent": "abuses",
                "response": "Cant enter more: endabuses"

            }
        }
    });

});

Meteor.methods({
    'getConversationResponse': function(response, conversationId, botState) {
        var conversation = Conversation.findOne({ "conversationId": conversationId })
        var states = conversation.states
        var intents = []
        var result = [0, 1]
        if (Object.keys(states[botState]).indexOf("userStates") != -1) {

            for (var j = 0; j < Object.keys(states[botState].userStates).length; j++) {
                intentO = {}
                intentO["intent"] = Object.keys(states[botState].userStates)[j]
                console.log("2", intentO["intent"])
                intentO['variations'] = []
                intentO['variations'] = states[botState].userStates[intentO["intent"]]
                intents.push(intentO)
            }
        }

        var data = {
            "queries": [
                response
            ],
            "intents": intents,
            "topn": 1,
            "method": "wordvector",
            "debug": 1,
            "wordVectorDistanceThreshold": 0
        };
        HTTP.call('POST', 'https://by8p8dgxqe.execute-api.us-east-1.amazonaws.com/devpoc/devpoc', {
            data: data,
            headers: {
                'content-type': "application/json",
                'cache-control': "no-cache"
            }
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                var matchedIntent = response.data[0].intentWithMaxScore

                console.log(response.data[0])
                // console.log(matchedIntent, states[matchedIntent].response);
                result = [matchedIntent, states[matchedIntent].response];
                BotState.update(BotState.findOne()._id, { $set: { "state": result } })
            }
        });
    }
});