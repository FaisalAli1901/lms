import { Random } from 'meteor/random'

Meteor.methods({
  saveImageFile: function(file, blob) {
    check(file, Object);
    check(blob, String);

    // SETUP:

    // TODO:Vignesh Move these configurations to public settings.
    var bucketName        = "lms-chi";
    var bucketUrl         = "https://s3-us-west-2.amazonaws.com/";
    var imageStore        = "images";

    var allowedImageExtensions = /\.(gif|jpg|jpeg|png)$/;
    var allowedImageTypes      = ["image/gif", "image/jpeg", "image/png"];
    var allowedImageSize       = 5242880;  // 5 MB

    // Simple filename cleanup function
    function cleanName(str) {
      if (str) { return slugify(str.replace(/\.\./g,'')); }
    }

    // CHECK:

    if (! allowedImageExtensions.test(file.name.toLowerCase())) { throw new Meteor.Error('Invalid image file extension.'); return; }
    if (allowedImageTypes.indexOf(file.type.toLowerCase()) < 0) { throw new Meteor.Error('Invalid image file type.'); return; }
    if (allowedImageSize < file.size) { throw new Meteor.Error('Invalid image file size.'); return; }

    // PROCESS:

    var serverFileName = Random.id() + '_' + cleanName(file.name);
    var key = imageStore + "/" + serverFileName;

    try {

      AWS.config.update({
        accessKeyId: "AKIAJOG6F6UN37YMNNLQ",
        secretAccessKey: "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"
      });

      var s3 = new AWS.S3();

      var res = s3.putObjectSync({
        "Bucket": bucketName,
        "ACL": 'public-read',
        "Body": new Buffer(blob, 'binary'),
        "ContentType": file.type,
        "Key": key
      });

    } catch(error) {
      throw new Meteor.Error(error.message);
    }

    return bucketUrl + bucketName + "/" + key;        // Return the full URL for the client to use
  }

});