import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {

});

Meteor.methods({
  createQuestion: (data) => {
    return Questions.insert(data);
  },
  deleteQuestion: (id) => {
    Questions.remove({ _id: id });
  },
  updateQuestion: (id, data) => {
    return Questions.update(id, {
      $set: data
    });
  }
});

Meteor.publish("questions", () => {
  return Questions.find();
});