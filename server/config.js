import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
	// Remove facebook config on startup
	ServiceConfiguration.configurations.remove({
		service: "facebook"
	});
	// Remove Google config on startup
	ServiceConfiguration.configurations.remove({
		service: "google"
	});

	// insert facebook config on startup
	// Development mode
	// ServiceConfiguration.configurations.insert({
	// 	service: "facebook",
	// 	appId: '481616815542342',
	// 	loginStyle: "popup",
	// 	secret: 'ced2776e8590a2c7ab4bbcdc44a9855f'
	// });
	// Production mode
	ServiceConfiguration.configurations.insert({
		service: "facebook",
		appId: '172960243262349',
		loginStyle: "popup",
		secret: '2e1c99c9bc13951b0b474226def19531'
	});

	// insert google config on startup
	ServiceConfiguration.configurations.insert({
		service: "google",
		clientId: '129373331311-tguha4qiatd0q0btkiuecoquljcprkmu.apps.googleusercontent.com',
		loginStyle: "popup",
		secret: 'S4LTyzmpj-2qcFbQYfgr7Kmq'
	});
});

Accounts.onCreateUser((options, user) => {

	if (user.services.facebook){
		console.log('facebook called');
		user.username = user.services.facebook.name;
		user.emails = [{ address: user.services.facebook.email }];
		user.profile = {
			pic: `http://graph.facebook.com/${user.services.facebook.id}/picture/?type=large`,
			firstName: user.services.facebook.first_name,
			lastName: user.services.facebook.last_name,
			gender: user.services.facebook.gender
		}

		return user;
	} else if (user.services.google) {
		console.log('google called');
		user.username = user.services.google.name;
		user.email = [{ address: user.services.google.email }];
		user.profile = {
			pic: user.services.google.picture,
			firstName: user.services.google.given_name,
			lastName: user.services.google.family_name
		}

		return user;
	} else{
		console.log(user, options);
		user.profile = options.profile
		return user
	}

});