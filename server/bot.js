import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {

});

Meteor.methods({
  createConversation(botSays) {
    return BotConversation.insert({
      states: {
        'start': {
          'response': botSays
        }
      }
    });
  },
  updateUserState(ConvId, tagcontent) {
    let temp = BotConversation.findOne({
      _id: ConvId
    });
    let id = temp._id

    BotConversation.update(id, { $set: { 'states.start.userStates': tagcontent } })
  },
  botRespondOnUser(Id, tags, tagContent) {
    let temp = BotConversation.findOne({
      _id: Id
    });
    let id = temp._id

    tags.forEach((tag) => {
      temp.states[tag] = tagContent[tag]

      BotConversation.update(id, { $set: { states: temp.states } })
    })
  },
  updateUserStateBot(Id, oldTags, tags, userStates) {
    let temp = BotConversation.findOne({
      _id: Id
    });
    let id = temp._id

    oldTags.forEach((tag) => {
      temp.states[tag].userStates = userStates

      BotConversation.update(id, { $set: { states: temp.states } })
    })
  },
  getConversation(Id, response, botStates) {
    let temp = BotConversation.findOne({
      _id: Id
    });
    let id = temp._id
    let botStateKey
    let keys = Object.keys(temp.states[botStates].userStates)
    keys.forEach((key) => {
      let states = temp.states[botStates].userStates[key]
      let botState = key
      states.forEach((state) => {
        let data = state.replace(/<[^>]*>/g, "")
        if (data === response) {
          botStateKey = botState
        }
      })
    })
    return botStateKey;
  }
});


Meteor.publish("BotConversation", () => {
  return BotConversation.find({});
});
