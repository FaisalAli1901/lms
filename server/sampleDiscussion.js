import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
	SampleDiscussion.remove({});

	SampleDiscussion.insert({
		authorId: '1',
		authorName: 'Josh Wright',
		createdAt: 'March 21 at 2:29pm',
		upVote: 12,
		downVote: 04,
		body: `This is purely a representation, again this is a way and not a real content and this is purely a representation, again this
				is a representation and not a real content and not a real content a real content.`,
		activeUsers: [
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
			{ userId: '1' },
			{ userId: '2' },
			{ userId: '3' },
		],
		noThreads: 24,
		noComments: 180,
		comments: [
			{ id: "1", name: 'Katie Wil', body: 'This is purely a representation, again this is a way and not a real content and this is purely a representation.'},
			{ id: "2", name: 'Katie Wil', body: 'This is purely a representation, again this is a way and not a real content and this is purely a representation.' }
		],
		liveComment: `Alex Wilson is commenting right now…`
	})
});