Router.configure({
    layoutTemplate: 'layout',
});
Router.route('/', function () {
  this.render('home');
});

Router.route('/quiz', function () {
  this.render('quiz');
});
Router.route('/quiz/:id', function() {
  this.render('quizTemp');
})

Router.route('/create-resource', function() {
  this.render('resourceCreate');
});
Router.route('/resource/:id', function() {
  this.render('resource');
});
Router.route('/resource/:id/add', function() {
  this.render('resourceUpdate');
});

Router.route('/chat/:id', function () {
  this.render('chatTemp');
});

Router.route('/dashboard', function () {
  this.render('dashboard');
});

Router.route('/discussions', function () {
  this.render('discussionList');
});

Router.route('/discussions/:did', function() {
  this.render('discussion');
});

Router.route('/discussions/:did/threads/:tid', function() {
  this.render('threadContainer');
});


Router.route('/login', {
	onBeforeAction: function(){
		if(!Meteor.userId()){
			this.render('login');
		}else{
			Router.go('/')
		}
	}
});
Router.route('/login/email', {
	onBeforeAction: function() {
		if (!Meteor.userId()) {
			this.render('loginWithEmail');
		} else {
			Router.go('/')
		}
	}
});
Router.route('/signup', {
	onBeforeAction: function() {
		if (!Meteor.userId()) {
			this.render('signup');
		} else {
			Router.go('/')
		}
	}
});

Router.route('/library', function() {
	this.render('resourceList');
})

Router.route('/debate/:id', function () {
	this.render('debate');
})

Router.route('/debate/:id/agree', function () {
	this.render('debateAgree');
})

Router.route('/debate/:id/disagree', function () {
	this.render('debateDisagree');
})

Router.route('/analytics', function () {
	this.render('analytics');
})
