Messages = new Meteor.Collection('messages');
Articles = new Meteor.Collection('articles');
Quizzes = new Meteor.Collection('quizzes');
Questions = new Meteor.Collection('questions');
Journaling = new Meteor.Collection('journaling');
BotState = new Meteor.Collection('botState');
Playlist = new Meteor.Collection('playlist');
Conversation = new Meteor.Collection('conversation');
Resource = new Meteor.Collection('resource');
CurrentTemplate = new Meteor.Collection('currentTemplate');
TemplateCollab = new Meteor.Collection('templateCollab');
TemplateCollab2 = new Meteor.Collection('templateCollab2');
SingleCanvas = new Meteor.Collection('singleCanvas');
UsersOnline = new Meteor.Collection('usersOnline');
CurrentChat = new Meteor.Collection('currentChat');
UsersId = new Meteor.Collection('UserId');
Canvas = new Meteor.Collection('canvas');
BotConversation = new Meteor.Collection('botConversation');
Discussion = new Meteor.Collection('discussion');
Thread = new Meteor.Collection('thread');
Comment = new Meteor.Collection('comment');
SampleDiscussion = new Meteor.Collection('sampleDiscussion');
SampleResources = new Meteor.Collection('sampleResources');
LP = new Meteor.Collection('lp');
Debate = new Meteor.Collection('debate');


// Discussion List
Discussion.list = function(options) {
  options = _.extend({ sort: { name: 1 }}, options);
  return Discussion.find({}, options);
};

// Discussion List
Discussion.list = function(options) {
  options = _.extend({ sort: { name: 1 }}, options);
  return Discussion.find({}, options);
};

// Images
var imageStore = new FS.Store.GridFS("images");

Images = new FS.Collection("images", {
  stores: [imageStore]
});
Images.deny({
  insert: function () {
    return false;
  },
  update: function () {
    return false;
  },
  remove: function () {
    return false;
  },
  download: function () {
    return false;
  }
});

Images.allow({
  insert: function () {
    return true;
  },
  update: function () {
    return true;
  },
  remove: function () {
    return true;
  },
  download: function () {
    return true;
  }
});